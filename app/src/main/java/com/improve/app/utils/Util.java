package com.improve.app.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.io.File;
import java.io.FileOutputStream;

/**
 * Created by   yudi rahmat
 * Email        yudirahmat7@gmail.com
 * Company      TRUSTUDIO
 */
public class Util {
    public static String[] months = { "Januari", "Februari", "March", "April",
            "Mei", "Juni", "Juli", "Augustus", "September", "Oktober",
            "November", "Desember", "" };

    public static String[] month = {"Januari", "Februari", "Maret", "April", "Mei", "Juni",
            "Juli", "Agustus", "September", "Oktober", "November", "Desember"};

    public static String[] categoryFilter = {"seminar", "lomba", "sertifikasi", "pameran", "pelatihan", "request event"};

    public static void getListViewSize(ListView myListView, int px) {
        ListAdapter myListAdapter = myListView.getAdapter();
        if (myListAdapter == null) {
            //do nothing return null
            return;
        }
        //set listAdapter in loop for getting final size
        int totalHeight = 0;
        for (int size = 0; size < myListAdapter.getCount(); size++) {
            View listItem = myListAdapter.getView(size, null, myListView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }
        //setting listview item in adapter
        ViewGroup.LayoutParams params = myListView.getLayoutParams();
        params.height = totalHeight + (myListView.getDividerHeight() * (myListAdapter.getCount() - 1)) + px;
        myListView.setLayoutParams(params);
    }

    public static void saveString(Context activity, String key, String value) {
        Log.d("SPMANAGER", "saving " + value + " for key " + key);
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(activity);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        if (editor.commit()) {
            Log.d("SPMANAGER", "commited " + value);
        } else {
            Log.d("SPMANAGER", "not commited");
        }
    }
    public static String getUserId(Context ctx) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(ctx);
        String value = sharedPreferences.getString(
                Cons.USER_ID, null);
        return value;
    }

    public static Bitmap decodeSampledBitmapFromUri(String path, int reqWidth, int reqHeight) {
        // First decode with inJustDecodeBounds = true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(path, options)	;
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight){
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;

        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round((float)height / (float)reqHeight);
            }else {
                inSampleSize = Math.round((float)width / (float)reqWidth);
            }
        }

        return inSampleSize;
    }

    public static String capitalLetter(String text) {
        String dataChanged = "";
        try {
            String[] mSplit = text.toLowerCase().split("\\ ");

            for (int i=0; i<mSplit.length; i++) {
                String data1 = mSplit[i].substring(0, 1).toUpperCase();
                String data2 = mSplit[i].substring(1, mSplit[i].length()).toLowerCase();
                String data3 = data1 + data2;

                dataChanged   += data3 + " ";
            }

        } catch (Exception e) {
            dataChanged = text;
            e.printStackTrace();
        }

        return dataChanged;
    }

    public static boolean saveBitmapToFile(String path, Bitmap bitmap) {
        File file 	= new File(path);

        if(file.exists()) file.delete();


        boolean res = false;

        if (!file.exists()) {
            try {
                FileOutputStream fos = new FileOutputStream(file);

                res = bitmap.compress(Bitmap.CompressFormat.JPEG, 90, fos);

                fos.close();
            } catch (Exception e) { }
        }

        return res;
    }

    public static String getPhotoName(String imagePath) {
        String fileName     = "";

        try {
            String[] fileSplit  = imagePath.split("\\/");

            fileName  = fileSplit[fileSplit.length - 1];
        } catch (Exception e) {
            e.printStackTrace();
        }

        return  fileName;
    }

    public static  String getFileType(String imagePath) {
        String fileType = "";

        try {
            String[] fileSplit  = getPhotoName(imagePath).split("\\.");

            fileType  = fileSplit[fileSplit.length - 1];
        } catch (Exception e) {
            e.printStackTrace();
        }

        return fileType;
    }

    public static String replaceString(String value, String oldChar, String newChar) {
        try {
            value   = value.replace(oldChar, newChar);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return value;
    }

    public static void createAppDir(String folder) {
        String extDir = Environment.getExternalStorageDirectory().getPath();
        String appDir = extDir + Cons.APP_DIR_OPEN + "/" + folder;

        File fappDir  = new File(appDir);

        fappDir.mkdirs();

        Log.i(Cons.TAG, "Create: " + appDir);
    }

    public static String getDir(String folder) {
        String extDir = Environment.getExternalStorageDirectory().getPath();
        String appDir = extDir + Cons.APP_DIR_OPEN + "/" + folder;

        return appDir;
    }
}

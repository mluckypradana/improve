package com.improve.app.utils;

/**
 * @author yudirahmat
 * @email yudirahmat7@gmail.com
 * @company trustudio
 */

public class Cons {
    public static final String TAG = "CELCOM";
    public static final String PRIVATE_PREF = "improve_Pref";

    public static final String APP_DIR_OPEN = "/Improve";

    /* Database */
    public static final int DB_VERSION = 1;
    public static final String DBNAME = "cektagihan.db";
    public static final String DBPATH = "/data/data/com.trustudio.android.telkomselasset/";
    public static final String DB_VERSION_NAME = "database_version";
    public static final String DBVER_KEY = "dbversion";
    public static final String PACKAGE_PREFIX = "";

    /* Connection */
    public static final String API_URL 					= "https://improveapp.herokuapp.com/";
    public static final String API_LOGIN			    = "api/sessions";
    public static final String API_REGISTER			    = "api/registrations";
    public static final String API_UPDATE_USER			= "api/sessions/update_user/";
    public static final String API_LIST_LOMBA	        = "api/contests";
    public static final String API_LIST_EVENT			= "api/events";
    public static final String API_LIST_PAMERAN			= "api/fairs";
    public static final String API_LIST_PELATIHAN   	= "api/trainings";
    public static final String API_LIST_SERTIFIKASI	    = "api/sertifications";
    public static final String API_REQUEST_EVENT	    = "api/request_events";
    public static final String API_CITIES	            = "api/request_events/get_cities/";
    public static final String API_LIST_CATEGORY        = "api/request_events/get_categories";
    public static final String API_LIST_CART            = "api/sessions/view_cart/";
    public static final String API_LIST_CANCEL_CART     = "api/sessions/celcel_cart";
    public static final String API_LIST_CHECKOUT_CART   = "api/sessions/checkout";

    //enable debug
    public static final boolean ENABLE_DEBUG            = true;
    public static final boolean ENABLE_DEVELOP          = false;

    //Shared Pref

    public static final String USER_ID = "user-id";

    //Type Program
    public static final String SEMINAR          = "seminar";
    public static final String LOMBA            = "lomba";
    public static final String PAMERAN          = "pameran";
    public static final String PELATIHAN        = "pelatihan";
    public static final String SERTIFIKASI      = "sertifikasi";

    //Font
    public static final String DEFAULT_FONT = "century-gothic.ttf";



    public static final String eventFilter(String type, String title, int cityId, String category) {
        String url = "api/request_events/get_data_filter?type=" + type;

        if(title != null)
            url += "&title=" + title;

        if(cityId > 0)
            url += "&city_id=" + cityId;

        if(category != null)
            url += "&category=" + category;

        return url;
    }
}
package com.improve.app.dao;

import java.util.List;

/**
 * Created by   Rian Erlangga Saputra
 * Email        rianerlangga03@gmail.com
 */
public class DetailPelatihan {
    public int id;
    public String title;
    public String title_description;
    public String category;
    public String description;
    public String place;
    public String price;
    public String available;
    public String registered;
    public String province_name;
    public String city_name;
    public String address;
    public String created_at;
    public String updated_at;
    public String creator;

    public String latitude;
    public String longitude;
    public String logo;
    public String poster_file_name;
    public List<Comments> comments;
    public List<TrainingSchedules> training_schedules;
    public List<TrainingFacilities> training_facilities;
    public List<TrainingTopics> training_topics;

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getTitle_description() {
        return title_description;
    }

    public void setTitle_description(String title_description) {
        this.title_description = title_description;
    }

    public List<TrainingSchedules> getTraining_schedules() {
        return training_schedules;
    }

    public void setTraining_schedules(List<TrainingSchedules> training_schedules) {
        this.training_schedules = training_schedules;
    }

    public List<TrainingFacilities> getTraining_facilities() {
        return training_facilities;
    }

    public void setTraining_facilities(List<TrainingFacilities> training_facilities) {
        this.training_facilities = training_facilities;
    }

    public List<TrainingTopics> getTraining_topics() {
        return training_topics;
    }

    public void setTraining_topics(List<TrainingTopics> training_topics) {
        this.training_topics = training_topics;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getAvailable() {
        return available;
    }

    public void setAvailable(String available) {
        this.available = available;
    }

    public String getRegistered() {
        return registered;
    }

    public void setRegistered(String registered) {
        this.registered = registered;
    }

    public String getProvince_name() {
        return province_name;
    }

    public void setProvince_name(String province_name) {
        this.province_name = province_name;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public List<Comments> getComments() {
        return comments;
    }

    public void setComments(List<Comments> comments) {
        this.comments = comments;
    }

    public String getPoster_file_name() {
        return poster_file_name;
    }

    public void setPoster_file_name(String poster_file_name) {
        this.poster_file_name = poster_file_name;
    }
}

package com.improve.app.dao;

/**
 * Created by RIAN ERLANGGA S on 31/01/2016.
 */
public class CreateCart {

    int id;
    String user_id_pemesan;
    String nama_pemesanmesan;
    String email_pemesan;
    String total;
    String state;
    String item_count;
    String category;
    String title;
    String created_at;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUser_id_pemesan() {
        return user_id_pemesan;
    }

    public void setUser_id_pemesan(String user_id_pemesan) {
        this.user_id_pemesan = user_id_pemesan;
    }

    public String getNama_pemesanmesan() {
        return nama_pemesanmesan;
    }

    public void setNama_pemesanmesan(String nama_pemesanmesan) {
        this.nama_pemesanmesan = nama_pemesanmesan;
    }

    public String getEmail_pemesan() {
        return email_pemesan;
    }

    public void setEmail_pemesan(String email_pemesan) {
        this.email_pemesan = email_pemesan;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getItem_count() {
        return item_count;
    }

    public void setItem_count(String item_count) {
        this.item_count = item_count;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }
}

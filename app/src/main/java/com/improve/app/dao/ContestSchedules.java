package com.improve.app.dao;

/**
 * Created by Rian on 27/01/2016.
 */
public class ContestSchedules {
    public String description;
    public String date_start;
    public String date_end;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate_start() {
        return date_start;
    }

    public void setDate_start(String date_start) {
        this.date_start = date_start;
    }

    public String getDate_end() {
        return date_end;
    }

    public void setDate_end(String date_end) {
        this.date_end = date_end;
    }
}

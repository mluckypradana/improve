package com.improve.app.dao;

import java.util.List;

/**
 * Created by   Rian Erlangga Saputra
 * Email        rianerlangga03@gmail.com
 */
public class DetailSertifikasi {
    public int id;
    public String title;
    public String category;
    public String description;
    public String price;
    public String available;
    public String registered;
    public String province_name;
    public String city_name;
    public String address;
    public String created_at;
    public String updated_at;
    public String creator;

    public String latitude;
    public String longitude;
    public String logo;
    public String poster;
    public List<Comments> comments;
    public List<SertificationDescription> sertification_programs;
    public List<SertificationSchedules> sertification_schedules;
    public List<SertificationDescription> sertification_requirements;
    public List<SertificationDescription> sertification_facilities;

    public List<SertificationDescription> getSertification_programs() {
        return sertification_programs;
    }

    public void setSertification_programs(List<SertificationDescription> sertification_programs) {
        this.sertification_programs = sertification_programs;
    }

    public List<SertificationSchedules> getSertification_schedules() {
        return sertification_schedules;
    }

    public void setSertification_schedules(List<SertificationSchedules> sertification_schedules) {
        this.sertification_schedules = sertification_schedules;
    }

    public List<SertificationDescription> getSertification_requirements() {
        return sertification_requirements;
    }

    public void setSertification_requirements(List<SertificationDescription> sertification_requirements) {
        this.sertification_requirements = sertification_requirements;
    }

    public List<SertificationDescription> getSertification_facilities() {
        return sertification_facilities;
    }

    public void setSertification_facilities(List<SertificationDescription> sertification_facilities) {
        this.sertification_facilities = sertification_facilities;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getAvailable() {
        return available;
    }

    public void setAvailable(String available) {
        this.available = available;
    }

    public String getRegistered() {
        return registered;
    }

    public void setRegistered(String registered) {
        this.registered = registered;
    }

    public String getProvince_name() {
        return province_name;
    }

    public void setProvince_name(String province_name) {
        this.province_name = province_name;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public List<Comments> getComments() {
        return comments;
    }

    public void setComments(List<Comments> comments) {
        this.comments = comments;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public class SertificationDescription {
        String description;

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }
    public class SertificationSchedules{
        String date_start;
        String date_end;

        public String getDate_end() {
            return date_end;
        }

        public void setDate_end(String date_end) {
            this.date_end = date_end;
        }

        public String getDate_start() {
            return date_start;
        }

        public void setDate_start(String date_start) {
            this.date_start = date_start;
        }
    }
}

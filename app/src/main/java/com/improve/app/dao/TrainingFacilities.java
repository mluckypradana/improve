package com.improve.app.dao;

/**
 * Created by Rian on 27/01/2016.
 */
public class TrainingFacilities {
    String facility_id;
    String description;

    public String getFacility_id() {
        return facility_id;
    }

    public void setFacility_id(String facility_id) {
        this.facility_id = facility_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

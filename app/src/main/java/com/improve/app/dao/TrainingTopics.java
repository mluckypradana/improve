package com.improve.app.dao;

/**
 * Created by Rian on 27/01/2016.
 */
public class TrainingTopics {
    String topic_id;
    String description;

    public String getTopic_id() {
        return topic_id;
    }

    public void setTopic_id(String topic_id) {
        this.topic_id = topic_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

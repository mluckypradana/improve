package com.improve.app.http;

import android.app.Activity;
import android.support.v4.app.Fragment;

import com.improve.app.utils.Cons;
import com.improve.app.utils.Debug;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;
import org.json.JSONTokener;

/**
 * Created by   yudi rahmat
 * Email        yudirahmat7@gmail.com
 */
public class RequestEventConnection {
    private RequestConnectionListener listenerRequest;

    public RequestEventConnection(Activity activity) {
        listenerRequest = (RequestConnectionListener) activity;
    }

    public RequestEventConnection(Fragment fragment) {
        listenerRequest = (RequestConnectionListener) fragment;
    }

    public interface RequestConnectionListener {
        void ResquestEventResponse(int status, String message);
    }

    public void requestEvent(String title, String description, String address, String userId, String latitude, String longitude, String category, String city, String otherCategory) {
        AsyncHttpClient client      = new AsyncHttpClient();
        RequestParams params        = new RequestParams();

        try {
            params.put("request_event[title]",              title);
            params.put("request_event[description]",        description);
            params.put("request_event[address]",            address);
            params.put("request_event[user_id]",            userId);
            params.put("request_event[longitude]",          longitude);
            params.put("request_event[latitude]",           latitude);
            params.put("request_event[another_category]",   otherCategory);
            params.put("request_event[category_id]",        category);
            params.put("request_event[city_id]",            city);
        } catch (Exception e) {
            e.printStackTrace();
        }

        final String ApiUrl         = Cons.API_URL + Cons.API_REQUEST_EVENT;

        client.post(ApiUrl, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, org.apache.http.Header[] headers, JSONObject response) {
                Debug.i(Cons.TAG, ApiUrl);
                Debug.i(Cons.TAG, response.toString());

                try {
                    JSONObject jsonObj              = (JSONObject) new JSONTokener(response.toString()).nextValue();
                    int status                      = jsonObj.getInt("status");

                    if (status == 1) {
                        listenerRequest.ResquestEventResponse(1, jsonObj.getString("message"));
                    } else {
                        listenerRequest.ResquestEventResponse(0, jsonObj.getString("message"));
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    listenerRequest.ResquestEventResponse(2, "Failed to Parse Json !");
                }

            }

            @Override
            public void onFailure(int statusCode, org.apache.http.Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                try {
                    JSONObject jsonObj = (JSONObject) new JSONTokener(errorResponse.toString()).nextValue();

                    listenerRequest.ResquestEventResponse(3, jsonObj.getString("message"));
                } catch (Exception e) {
                    e.printStackTrace();
                    listenerRequest.ResquestEventResponse(3, "Gagal Mendapatkan Data !");
                }

                //Debug.i(Cons.TAG, "error data = " + errorResponse.toString());
            }
        });
    }
}

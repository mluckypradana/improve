package com.improve.app.http;

import android.support.v4.app.Fragment;

import com.improve.app.model.EventDates;
import com.improve.app.model.EventFacilitys;
import com.improve.app.model.EventFoto;
import com.improve.app.model.EventPelatihan;
import com.improve.app.model.EventSchedule;
import com.improve.app.model.EventPelatihan;
import com.improve.app.model.EventTopics;
import com.improve.app.utils.Cons;
import com.improve.app.utils.Debug;
import com.improve.app.utils.Util;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;

/**
 * Created by maya on 11/23/2015.
 * Created by   yudi rahmat
 * Email        yudirahmat7@gmail.com
 */
public class PelatihanConnection {
    private EventsConnectionListener listenerVoucher;

    public PelatihanConnection(Fragment fragment) {
        listenerVoucher = (EventsConnectionListener) fragment;
    }

    public interface EventsConnectionListener {
        void EventsConnection(int status, String message, ArrayList<EventPelatihan> mEvents);
    }

    public void Events(String category, final boolean filter, String title, int cityId, String category2) {
        AsyncHttpClient client      = new AsyncHttpClient();
        RequestParams params        = new RequestParams();

        try {
            //params.put("category", category);

            if(title != null)
                params.put("title",     title);

            if(cityId > 0)
                params.put("city_id",   cityId);

            if(category2 != null)
                params.put("category",  category2);
        } catch (Exception e) {
            e.printStackTrace();
        }

        title           = Util.replaceString(title, " ", "%20");
        String ApiUrl   = Cons.API_URL + Cons.API_LIST_PELATIHAN;

        if(filter == true) {
            ApiUrl = Cons.API_URL + Cons.eventFilter(category, title, cityId, category2);
        }

        Debug.i(Cons.TAG, ApiUrl);

        client.get(ApiUrl, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, org.apache.http.Header[] headers, JSONObject response) {
                if(filter == false)
                    onSuccessData(response);
                else
                    onSuccessDataFilter(response);
            }

            @Override
            public void onFailure(int statusCode, org.apache.http.Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                onFailureData(errorResponse);

            }
        });
    }

    private void onFailureData(JSONObject errorResponse) {
        try {
            JSONObject jsonObj = (JSONObject) new JSONTokener(errorResponse.toString()).nextValue();

            listenerVoucher.EventsConnection(3, jsonObj.getString("message"), null);
        } catch (Exception e) {
            e.printStackTrace();
            listenerVoucher.EventsConnection(3, "Gagal Mendapatkan Data !", null);
        }
    }

    private void onSuccessData(JSONObject response) {
        final ArrayList<EventPelatihan> mEvents         = new ArrayList<EventPelatihan>();

        Debug.i(Cons.TAG, response.toString());

        try {
            JSONObject jsonObj                  = (JSONObject) new JSONTokener(response.toString()).nextValue();
            int status                          = jsonObj.getInt("status");

            if (status == 1) {
                JSONArray jsonArr               = jsonObj.getJSONArray("data");

                //JSONObject json                 = jsonObj.getJSONObject("data");
                for(int i=0; i<jsonArr.length(); i++) {
                    JSONObject json             = jsonArr.getJSONObject(i);

                    EventPelatihan Events       = new EventPelatihan();

                    Events.id                   = json.getInt("id");
                    Events.category             = jsonObj.getString("category");

                    Events.title                = json.getString("title");
                    Events.description          = json.getString("description");
                    Events.price                = json.getString("price");
                    Events.available            = json.getString("available");
                    Events.registered           = json.getString("registered");
                    Events.province_name        = json.getString("province_name");
                    Events.city_name            = json.getString("city_name");
                    Events.address              = json.getString("address");
                    Events.created_at           = json.getString("created_at");
                    Events.updated_at           = json.getString("updated_at");
                    Events.creator              = json.getString("creator");
                    Events.comments             = json.getInt("comments");
                    Events.logo                 = json.getString("logo");
                    Events.place                = json.getString("place");
                    Events.poster_file_name     = json.getString("poster_file_name");
                    Events.category_program     = json.getString("category_program");

                    //Events.latitude           = json.getString("latitude");
                    //Events.longitude          = json.getString("longitude");

                    JSONArray ArrSchedule       = json.getJSONArray("training_schedules");
                    for(int j=0; j<ArrSchedule.length(); j++) {
                        JSONObject jsonDate     = ArrSchedule.getJSONObject(j);

                        EventSchedule data      = new EventSchedule();

                        data.description        = jsonDate.getString("description");
                        data.date_start         = jsonDate.getString("date_start");
                        data.date_end           = jsonDate.getString("date_end");

                        Events.listSchedule.add(data);
                    }

                    JSONArray ArrFacility       = json.getJSONArray("training_facilities");
                    for(int j=0; j<ArrFacility.length(); j++) {
                        JSONObject jsonFacility = ArrFacility.getJSONObject(j);

                        EventFacilitys data     = new EventFacilitys();

                        data.count              = jsonFacility.getInt("facility_id");
                        data.facility           = jsonFacility.getString("description");

                        Events.listFacilitys.add(data);
                    }

                    JSONArray ArrTopics         = json.getJSONArray("training_topics");
                    for(int j=0; j<ArrFacility.length(); j++) {
                        JSONObject jsonTopics   = ArrTopics.getJSONObject(j);

                        EventTopics data        = new EventTopics();

                        data.topic_id           = jsonTopics.getInt("topic_id");
                        data.description        = jsonTopics.getString("description");

                        Events.listTopics.add(data);
                    }

                    mEvents.add(Events);
                }

                listenerVoucher.EventsConnection(1, jsonObj.getString("message"), mEvents);
            } else {
                listenerVoucher.EventsConnection(0, jsonObj.getString("message"), null);
            }

        } catch (Exception e) {
            e.printStackTrace();
            listenerVoucher.EventsConnection(2, "Failed to Parse Json !", null);
        }
    }

    private void onSuccessDataFilter(JSONObject response) {
        final ArrayList<EventPelatihan> mEvents    = new ArrayList<EventPelatihan>();
        ArrayList<EventDates> listDate           = new ArrayList<>();
        ArrayList<EventFacilitys> listFacilitys  = new ArrayList<>();
        ArrayList<EventFoto> listFoto            = new ArrayList<>();

        Debug.i(Cons.TAG, response.toString());

        try {
            JSONObject jsonObj                  = (JSONObject) new JSONTokener(response.toString()).nextValue();
            int status                          = jsonObj.optInt("status");

            if (status == 1) {
                JSONArray jsonArr               = jsonObj.getJSONArray("data");

                //JSONObject json                 = jsonObj.getJSONObject("data");
                for(int i=0; i<jsonArr.length(); i++) {
                    JSONObject json             = jsonArr.getJSONObject(i);

                    EventPelatihan Events     = new EventPelatihan();

                    Events.id                   = json.optInt("id");
                    Events.category             = jsonObj.optString("category");

                    Events.title                = json.optString("title");
                    Events.description          = json.optString("description");
                    Events.price                = json.optString("price");
                    Events.available            = json.optString("available");
                    Events.registered           = json.optString("registered");
                    Events.province_name        = json.optString("province_name");
                    Events.city_name            = json.optString("city_name");
                    Events.address              = json.optString("address");
                    Events.created_at           = json.optString("created_at");
                    Events.updated_at           = json.optString("updated_at");
                    Events.creator              = json.optString("creator");
                    Events.comments             = json.optInt("comments");
                    Events.logo                 = json.optString("logo");
                    Events.category_program     = json.optString("category_program");

                    mEvents.add(Events);
                }

                listenerVoucher.EventsConnection(1, jsonObj.optString("message"), mEvents);
            } else {
                listenerVoucher.EventsConnection(0, jsonObj.optString("message"), null);
            }

        } catch (Exception e) {
            e.printStackTrace();
            listenerVoucher.EventsConnection(2, "Failed to Parse Json !", null);
        }
    }

}

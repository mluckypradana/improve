package com.improve.app.http;

import android.app.Activity;
import android.support.v4.app.Fragment;

import com.improve.app.model.CitiesModel;
import com.improve.app.model.EventSchedule;
import com.improve.app.utils.Cons;
import com.improve.app.utils.Debug;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;

/**
 * Created by maya on 11/23/2015.
 * Created by   yudi rahmat
 * Email        yudirahmat7@gmail.com
 */
public class CityConnection {
    private CitiesConnectionListener listenerCitities;

    public CityConnection(Fragment fragment) {
        listenerCitities = (CitiesConnectionListener) fragment;
    }

    public CityConnection(Activity fragment) {
        listenerCitities = (CitiesConnectionListener) fragment;
    }

    public interface CitiesConnectionListener {
        void EventsConnection(int status, String message, ArrayList<CitiesModel> mCities);
    }

    public void getCities(final int cityId) {
        AsyncHttpClient client      = new AsyncHttpClient();
        RequestParams params        = new RequestParams();

        String ApiUrl               = Cons.API_URL + Cons.API_CITIES + cityId;

        if(cityId < 0)
            ApiUrl = Cons.API_URL + Cons.API_LIST_CATEGORY;

        Debug.i(Cons.TAG, ApiUrl);

        client.get(ApiUrl, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, org.apache.http.Header[] headers, JSONObject response) {
                final ArrayList<CitiesModel> mCities    = new ArrayList<CitiesModel>();

                Debug.i(Cons.TAG, response.toString());

                try {
                    JSONObject jsonObj                  = (JSONObject) new JSONTokener(response.toString()).nextValue();
                    int status                          = jsonObj.getInt("status");

                    if (status == 1) {
                        JSONArray jsonArr               = jsonObj.getJSONArray("data");

                        for(int i=0; i<jsonArr.length(); i++) {
                            JSONObject json             = jsonArr.getJSONObject(i);

                            CitiesModel city            = new CitiesModel();

                            city.id                     = json.getInt("id");
                            city.name                   = json.getString("name");
                            city.category               = cityId;

                            mCities.add(city);
                        }

                        listenerCitities.EventsConnection(1, jsonObj.getString("message"), mCities);
                    } else {
                        listenerCitities.EventsConnection(0, jsonObj.getString("message"), null);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    listenerCitities.EventsConnection(2, "Failed to Parse Json !", null);
                }

            }

            @Override
            public void onFailure(int statusCode, org.apache.http.Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                try {
                    JSONObject jsonObj = (JSONObject) new JSONTokener(errorResponse.toString()).nextValue();

                    listenerCitities.EventsConnection(3, jsonObj.getString("message"), null);
                } catch (Exception e) {
                    e.printStackTrace();
                    listenerCitities.EventsConnection(3, "Gagal Mendapatkan Data !", null);
                }

            }
        });
    }
}

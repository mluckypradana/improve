package com.improve.app.http;

import android.app.Activity;

import com.improve.app.model.User;
import com.improve.app.utils.Cons;
import com.improve.app.utils.Debug;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;

/**
 * Created by   yudi rahmat
 * Email        yudirahmat7@gmail.com
 */
public class LoginConnection {
    private LoginConnectionListener listenerVoucher;

    public LoginConnection(Activity activity) {
        listenerVoucher = (LoginConnectionListener) activity;
    }

    public interface LoginConnectionListener {
        void LoginConnection(int status, String message, ArrayList<User> mLogin);
    }

    public void login(String username, String password) {
        AsyncHttpClient client      = new AsyncHttpClient();
        RequestParams params        = new RequestParams();

        try {
            params.put("user[email]",           username);
            params.put("user[password]",        password);
        } catch (Exception e) {
            e.printStackTrace();
        }

        final String ApiUrl         = Cons.API_URL + Cons.API_LOGIN;

        client.post(ApiUrl, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, org.apache.http.Header[] headers, JSONObject response) {
                final ArrayList<User> mLogin = new ArrayList<User>();

                Debug.i(Cons.TAG, ApiUrl);
                Debug.i(Cons.TAG, response.toString());

                try {
                    JSONObject jsonObj              = (JSONObject) new JSONTokener(response.toString()).nextValue();
                    int status                      = jsonObj.getInt("status");

                    if (status == 1) {
                        JSONObject json             = jsonObj.getJSONObject("data");

                        User data                   = new User();

                        data.id                     = json.getInt("id");
                        data.email                  = json.optString("email");
                        data.created_at             = json.optString("created_at");
                        data.updated_at             = json.optString("updated_at");
                        data.role_id                = json.optString("role_id");
                        data.role_name              = json.optString("role_name");
                        data.username               = json.optString("username");
                        data.birthday               = json.optString("birthday");
                        data.phone                  = json.optString("phone");
                        data.address                = json.optString("address");
                        data.province               = json.optString("province");
                        data.city                   = json.optString("city");
                        data.photo                  = json.optString("photo");

                        mLogin.add(data);

                        listenerVoucher.LoginConnection(1, jsonObj.optString("errors"), mLogin);
                    } else {
                        listenerVoucher.LoginConnection(0, jsonObj.optString("errors"), null);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    listenerVoucher.LoginConnection(2, "Failed to Parse Json !", null);
                }
            }

            @Override
            public void onFailure(int statusCode, org.apache.http.Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                try {
                    JSONObject jsonObj = (JSONObject) new JSONTokener(errorResponse.toString()).nextValue();

                    listenerVoucher.LoginConnection(3, jsonObj.optString("message"), null);
                } catch (Exception e) {
                    e.printStackTrace();
                    listenerVoucher.LoginConnection(3, "Gagal Mendapatkan Data !", null);
                }

                //Debug.i(Cons.TAG, "error data = " + errorResponse.toString());
            }
        });
    }
}

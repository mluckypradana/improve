package com.improve.app.http;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;

import com.improve.app.model.User;
import com.improve.app.utils.Cons;
import com.improve.app.utils.Debug;
import com.improve.app.utils.Util;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.File;
import java.io.FileNotFoundException;

/**
 * Created by   yudi rahmat
 * Email        yudirahmat7@gmail.com
 */
public class RegisterConnection {
    private final Activity activity;
    private RegisterConnectionListener listenerRegist;

    public RegisterConnection(Activity activity) {
        listenerRegist = (RegisterConnectionListener) activity;
        this.activity = activity;
    }

    public RegisterConnection(Fragment fragment) {
        listenerRegist = (RegisterConnectionListener) fragment;
        this.activity = fragment.getActivity();
    }

    public interface RegisterConnectionListener {
        void LoginConnection(int status, String message);
    }

    /**
     //* @param isUpdate  put false if it is register
     //* @param id        Put zero if it is register
     * @param imagePath Put "" if no image path
     * @param email     Email of user
     * @param city      City of user
     * @param address   User address
     * @param phone     User phone
     * @param birthday  User birthday
     * @param username  User's username
     * @param password  User's password
     */
    public void regisetrOrUpdate(final boolean isUpdate, Integer id, String imagePath, String email, String city, String address, String phone, String birthday, String username, String password) {
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();

        SharedPreferences mSharedPreferences = activity.getSharedPreferences(Cons.PRIVATE_PREF, Context.MODE_PRIVATE);


        if(imagePath == null || imagePath.equals("")) {
            try {
                params.put("user[email]",       email);
                params.put("user[password]",    password);
                params.put("user[username]",    username);
                params.put("user[city]",        city);
                params.put("user[city_id]",     city);
                params.put("user[address]",     address);
                params.put("user[phone]",       phone);
                params.put("user[birthday]",    birthday);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            File file = new File(imagePath);

            try {
                params.put("user[email]",       email);
                params.put("user[password]",    password);
                params.put("user[username]",    username);
                params.put("user[city]",        city);
                params.put("user[city_id]",     city);
                params.put("user[address]",     address);
                params.put("user[phone]",       phone);
                params.put("user[birthday]",    birthday);
                params.put("user[photo_content_type]", Util.getFileType(imagePath));
                params.put("user[photo]",       file);
                params.put("photo",             file);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        String ApiUrl   = Cons.API_URL + Cons.API_REGISTER;

        if(isUpdate == true) {
            ApiUrl      = Cons.API_URL + "api/sessions/update_user/" + mSharedPreferences.getInt(Cons.PACKAGE_PREFIX + "user_id", 0);
        }

        Debug.i(Cons.TAG, ApiUrl);

        client.post(ApiUrl, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, org.apache.http.Header[] headers, JSONObject response) {
                onSuccessData(response);
            }

            @Override
            public void onFailure(int statusCode, org.apache.http.Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                try {
                    JSONObject jsonObj = (JSONObject) new JSONTokener(errorResponse.toString()).nextValue();

                    listenerRegist.LoginConnection(3, jsonObj.optString("message"));
                } catch (Exception e) {
                    e.printStackTrace();
                    listenerRegist.LoginConnection(3, "Gagal Mendapatkan Data !");
                }

            }
        });
    }

    private void onSuccessData(JSONObject response) {
        Debug.i(Cons.TAG, response.toString());

        try {
            JSONObject jsonObj = (JSONObject) new JSONTokener(response.toString()).nextValue();
            int status = jsonObj.optInt("status");

            if (status == 1) {
                User users = new User();

                JSONObject json         = jsonObj.getJSONObject("data");
                users.id                = json.optInt("id");
                users.email             = json.optString("email");
                users.created_at        = json.optString("created_at");
                users.updated_at        = json.optString("updated_at");
                users.role_id           = json.optString("role_id");
                //users.role_name       = json.optString("role_name");
                users.username          = json.optString("username");
                users.birthday          = json.optString("birthday");
                users.phone             = json.optString("phone");
                users.address           = json.optString("address");
                //users.province        = json.optString("province");
                users.city              = json.optString("city_id");
                users.photo             = json.optString("photo_file_name");

                SharedPreferences mSharedPreferences = activity.getSharedPreferences(Cons.PRIVATE_PREF, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = mSharedPreferences.edit();

                editor.putString(Cons.PACKAGE_PREFIX + "user_email",            users.email);
                editor.putInt(Cons.PACKAGE_PREFIX + "user_id",                  users.id);
                editor.putString(Cons.PACKAGE_PREFIX + "user_created_at",       users.created_at);
                editor.putString(Cons.PACKAGE_PREFIX + "user_updated_at",       users.updated_at);
                editor.putString(Cons.PACKAGE_PREFIX + "user_role_id",          users.role_id);
                //editor.putString(Cons.PACKAGE_PREFIX + "user_role_name",      users.role_name);
                editor.putString(Cons.PACKAGE_PREFIX + "user_username",         users.username);
                editor.putString(Cons.PACKAGE_PREFIX + "user_birthday",         users.birthday);
                editor.putString(Cons.PACKAGE_PREFIX + "user_phone",            users.phone);
                editor.putString(Cons.PACKAGE_PREFIX + "user_address",          users.address);
                //editor.putString(Cons.PACKAGE_PREFIX + "user_province",       users.province);
                editor.putString(Cons.PACKAGE_PREFIX + "user_city",             users.city);
                editor.putString(Cons.PACKAGE_PREFIX + "user_photo",            users.photo);
                editor.commit();

                listenerRegist.LoginConnection(1, jsonObj.optString("errors"));
            } else {
                listenerRegist.LoginConnection(0, jsonObj.optString("errors"));
            }

        } catch (Exception e) {
            e.printStackTrace();
            listenerRegist.LoginConnection(2, "Failed to Parse Json !");
        }
    }
}

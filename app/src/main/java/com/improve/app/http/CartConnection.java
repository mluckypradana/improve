package com.improve.app.http;

import android.support.v4.app.Fragment;

import com.improve.app.model.Comments;
import com.improve.app.model.EventDates;
import com.improve.app.model.EventFacilitys;
import com.improve.app.model.EventFoto;
import com.improve.app.model.CartModel;
import com.improve.app.utils.Cons;
import com.improve.app.utils.Debug;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;

/**
 * Created by maya on 11/23/2015.
 * Created by   yudi rahmat
 * Email        yudirahmat7@gmail.com
 */
public class CartConnection {
    private CartConnectionListener listenerVoucher;

    public CartConnection(Fragment fragment) {
        listenerVoucher = (CartConnectionListener) fragment;
    }

    public interface CartConnectionListener {
        void CartConnection(int status, String message, ArrayList<CartModel> mEvents);
        void CancelCartConnection(int status, String message);
        void CheckoutCartConnection(int status, String messaage);
    }

    public void cart(int userId) {
        AsyncHttpClient client      = new AsyncHttpClient();
        RequestParams params        = new RequestParams();

        final String ApiUrl         = Cons.API_URL + Cons.API_LIST_CART + userId;

        client.get(ApiUrl, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, org.apache.http.Header[] headers, JSONObject response) {
                final ArrayList<CartModel> mCart = new ArrayList<CartModel>();

                Debug.i(Cons.TAG, ApiUrl);
                Debug.i(Cons.TAG, response.toString());

                try {
                    JSONObject jsonObj = (JSONObject) new JSONTokener(response.toString()).nextValue();
                    int status = jsonObj.getInt("status");

                    if (status == 1) {
                        JSONArray jsonArr = jsonObj.getJSONArray("data");

                        for (int i = 0; i < jsonArr.length(); i++) {
                            JSONObject json = jsonArr.getJSONObject(i);

                            CartModel cart = new CartModel();

                            cart.id                 = json.getInt("id");
                            cart.nama_pemesan       = json.getString("nama_pemesan");
                            cart.email_pemesan      = json.getString("email_pemesan");
                            cart.total              = json.getInt("total");
                            cart.state              = json.getString("state");
                            cart.item_count         = json.getString("item_count");
                            cart.program_id         = json.getInt("program_id");
                            cart.category           = json.getString("category");
                            cart.title              = json.getString("title");

                            mCart.add(cart);
                        }

                        listenerVoucher.CartConnection(1, jsonObj.getString("message"), mCart);
                    } else {
                        listenerVoucher.CartConnection(0, jsonObj.getString("message"), null);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    listenerVoucher.CartConnection(2, "Failed to Parse Json !", null);
                }

            }

            @Override
            public void onFailure(int statusCode, org.apache.http.Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                try {
                    JSONObject jsonObj = (JSONObject) new JSONTokener(errorResponse.toString()).nextValue();

                    listenerVoucher.CartConnection(3, jsonObj.getString("message"), null);
                } catch (Exception e) {
                    e.printStackTrace();
                    listenerVoucher.CartConnection(3, "Gagal Mendapatkan Data !", null);
                }

            }
        });
    }

    public void cancelCart(int userId, String typeProgram, int programId) {
        AsyncHttpClient client      = new AsyncHttpClient();
        RequestParams params        = new RequestParams();

        try {
            params.put("order[user_id]",        userId);
            params.put("order[type_program]",   typeProgram);
            params.put("order[program_id]",     programId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Debug.i(Cons.TAG, "response " + userId + " -- " + typeProgram + " -- " + programId);

        final String ApiUrl         = Cons.API_URL + Cons.API_LIST_CANCEL_CART;

        client.post(ApiUrl, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, org.apache.http.Header[] headers, JSONObject response) {
                Debug.i(Cons.TAG, ApiUrl);
                Debug.i(Cons.TAG, response.toString());

                try {
                    JSONObject jsonObj = (JSONObject) new JSONTokener(response.toString()).nextValue();
                    int status = jsonObj.getInt("status");

                    listenerVoucher.CancelCartConnection(status, jsonObj.getString("message"));

                } catch (Exception e) {
                    e.printStackTrace();
                    listenerVoucher.CancelCartConnection(2, "Failed to Parse Json !");
                }

            }

            @Override
            public void onFailure(int statusCode, org.apache.http.Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                try {
                    JSONObject jsonObj = (JSONObject) new JSONTokener(errorResponse.toString()).nextValue();

                    listenerVoucher.CancelCartConnection(3, jsonObj.getString("message"));
                } catch (Exception e) {
                    e.printStackTrace();
                    listenerVoucher.CancelCartConnection(3, "Gagal Mendapatkan Data !");
                }

            }
        });
    }

    public void checkoutCart(int userId) {
        AsyncHttpClient client      = new AsyncHttpClient();
        RequestParams params        = new RequestParams();

        final String ApiUrl         = Cons.API_URL + Cons.API_LIST_CHECKOUT_CART + "/" + userId;

        client.get(ApiUrl, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, org.apache.http.Header[] headers, JSONObject response) {
                Debug.i(Cons.TAG, ApiUrl);
                Debug.i(Cons.TAG, response.toString());

                try {
                    JSONObject jsonObj = (JSONObject) new JSONTokener(response.toString()).nextValue();
                    int status = jsonObj.getInt("status");

                    listenerVoucher.CheckoutCartConnection(status, jsonObj.getString("message"));

                } catch (Exception e) {
                    e.printStackTrace();
                    listenerVoucher.CheckoutCartConnection(2, "Failed to Parse Json !");
                }

            }

            @Override
            public void onFailure(int statusCode, org.apache.http.Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                try {
                    JSONObject jsonObj = (JSONObject) new JSONTokener(errorResponse.toString()).nextValue();

                    listenerVoucher.CheckoutCartConnection(3, jsonObj.getString("message"));
                } catch (Exception e) {
                    e.printStackTrace();
                    listenerVoucher.CheckoutCartConnection(3, "Gagal Mendapatkan Data !");
                }

            }
        });
    }
}

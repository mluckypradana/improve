package com.improve.app.service.response;

import com.improve.app.model.CitiesModel;

import java.util.List;

/**
 * Created by Rian on 23/01/2016.
 */
public class CitiesResponse extends BaseResponse {
    private List<CitiesModel> data;

    public List<CitiesModel> getData() {
        return data;
    }

    public void setData(List<CitiesModel> data) {
        this.data = data;
    }
}

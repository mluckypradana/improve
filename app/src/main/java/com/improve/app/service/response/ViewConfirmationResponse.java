package com.improve.app.service.response;

import com.improve.app.model.ViewConfirmation;

import java.util.List;

/**
 * Created by Rian on 23/01/2016.
 */
public class ViewConfirmationResponse extends BaseResponse {
    String category;
    List<ViewConfirmation> data;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<ViewConfirmation> getData() {
        return data;
    }

    public void setData(List<ViewConfirmation> data) {
        this.data = data;
    }
}

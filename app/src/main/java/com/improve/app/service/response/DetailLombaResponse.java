package com.improve.app.service.response;

import com.improve.app.dao.DetailLomba;

import java.util.List;

/**
 * Created by Rian on 23/01/2016.
 */
public class DetailLombaResponse extends BaseResponse {
    String category;
    List<DetailLomba> data;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<DetailLomba> getData() {
        return data;
    }

    public void setData(List<DetailLomba> data) {
        this.data = data;
    }
}

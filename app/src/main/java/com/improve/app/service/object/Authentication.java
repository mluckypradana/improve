package com.improve.app.service.object;

/**
 * Created by RianErlangga on 17-Oct-15.
 */
public class Authentication {
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}

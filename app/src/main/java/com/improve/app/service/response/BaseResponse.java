package com.improve.app.service.response;

/**
 * Created by RianErlangga on 17-Oct-15.
 */
public class BaseResponse {

    int status;
    String message;
    String category;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}

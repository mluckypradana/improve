package com.improve.app.service.response;

import com.improve.app.dao.DetailPelatihan;

import java.util.List;

/**
 * Created by Rian on 23/01/2016.
 */
public class DetailPelatihanResponse extends BaseResponse {
    String category;
    List<DetailPelatihan> data;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<DetailPelatihan> getData() {
        return data;
    }

    public void setData(List<DetailPelatihan> data) {
        this.data = data;
    }
}

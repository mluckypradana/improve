package com.improve.app.service.response;

import com.improve.app.model.User;

/**
 * Created by Rian on 23/01/2016.
 */
public class UserDetailResponse extends BaseResponse {
    private User data;

    public User getData() {
        return data;
    }

    public void setData(User data) {
        this.data = data;
    }
}

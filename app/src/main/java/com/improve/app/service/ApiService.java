package com.improve.app.service;


import com.improve.app.service.response.CitiesResponse;
import com.improve.app.service.response.CreateCartResponse;
import com.improve.app.service.response.DetailLombaResponse;
import com.improve.app.service.response.DetailPameranResponse;
import com.improve.app.service.response.DetailPelatihanResponse;
import com.improve.app.service.response.DetailSeminarResponse;
import com.improve.app.service.response.DetailSertifikasiResponse;
import com.improve.app.service.response.UserDetailResponse;
import com.improve.app.service.response.ViewConfirmationResponse;
import com.squareup.okhttp.RequestBody;

import java.util.Map;

import retrofit.Call;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.PartMap;
import retrofit.http.Path;

/**
 * Created by MuhammadLucky on 6/4/2015.
 * Class
 */
public interface ApiService {

    //API NO 2
    @GET("api/events/{id}")
    public Call<DetailSeminarResponse> get_detail_seminar(@Path("id") String id);

    @GET("api/contests/{id}")
    public Call<DetailLombaResponse> get_detail_lomba(@Path("id") String id);

    @GET("api/fairs/{id}")
    public Call<DetailPameranResponse> get_detail_pameran(@Path("id") String id);

    @GET("api/sertifications/{id}")
    public Call<DetailSertifikasiResponse> get_detail_sertifikasi(@Path("id") String id);

    @GET("api/trainings/{id}")
    public Call<DetailPelatihanResponse> get_detail_pelatihan(@Path("id") String id);

    /**
     * id = 0 get provinces
     * id != 0 get cities based on province id
     * @param id
     * @return
     */
    @GET("api/request_events/get_cities/{id}")
    public Call<CitiesResponse> getProvincesOrCities(@Path("id") Integer id);

    @FormUrlEncoded
    @POST("api/sessions/create_cart")
    public Call<CreateCartResponse> create_cart(
            @Field("order[user_id]") String user_id,
            @Field("order[item_count]") String item_count,
            @Field("order[type_program]") String type_program,
            @Field("order[program_id]") String program_id);

    @FormUrlEncoded
    @POST("api/sessions/create_confirmation")
    public Call<ViewConfirmationResponse> payment_confirmation(
            @Field("payment[user_id]") String user_id,
            @Field("payment[no_order]") String no_order,
            @Field("payment[payment_method]") String payment_method,
            @Field("payment[amount]") String amount);

    @GET("api/sessions/view_confirmation/{id}")
    public Call<ViewConfirmationResponse> get_view_confirmation(@Path("id") String id);

    @GET("api/sessions/detail_user/{id}")
    public Call<UserDetailResponse> getUserDetail(@Path("id") int id);

    @Multipart
    @POST("/api/sessions/update_user/{id}")
    public Call<UserDetailResponse> updateUser(
            @Path("id") int id,
            @PartMap Map<String, RequestBody> params
    );
}

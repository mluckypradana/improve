package com.improve.app.service.response;

import com.improve.app.dao.SeminarDao;

import java.util.List;

/**
 * Created by Rian on 23/01/2016.
 */
public class DetailSeminarResponse extends BaseResponse {
    String category;
    List<SeminarDao> data;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<SeminarDao> getData() {
        return data;
    }

    public void setData(List<SeminarDao> data) {
        this.data = data;
    }
}

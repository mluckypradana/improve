package com.improve.app.service.response;

import com.improve.app.dao.DetailPameran;

import java.util.List;

/**
 * Created by Rian on 23/01/2016.
 */
public class DetailPameranResponse extends BaseResponse {
    String category;
    List<DetailPameran> data;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<DetailPameran> getData() {
        return data;
    }

    public void setData(List<DetailPameran> data) {
        this.data = data;
    }
}

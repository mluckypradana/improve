package com.improve.app.service.response;

import com.improve.app.dao.DetailSertifikasi;

import java.util.List;

/**
 * Created by Rian on 23/01/2016.
 */
public class DetailSertifikasiResponse extends BaseResponse {
    String category;
    List<DetailSertifikasi> data;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<DetailSertifikasi> getData() {
        return data;
    }

    public void setData(List<DetailSertifikasi> data) {
        this.data = data;
    }
}

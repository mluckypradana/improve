package com.improve.app.service.response;

import com.improve.app.dao.CreateCart;

import java.util.List;

/**
 * Created by Rian on 23/01/2016.
 */
public class CreateCartResponse extends BaseResponse {
    String category;
    List<CreateCart> data;

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public void setMessage(String message) {
        this.message = message;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<CreateCart> getData() {
        return data;
    }

    public void setData(List<CreateCart> data) {
        this.data = data;
    }
}

package com.improve.app.model;

/**
 * Created by Yudi on 2/8/2016.
 */
public class CitiesModel extends BaseModel {
    public int id;
    public String name;
    public int category;
    public Integer province_id;
}

package com.improve.app.model;

import java.util.ArrayList;

/**
 * Created by maya on 11/28/2015.
 * Created by   yudi rahmat
 * Email        yudirahmat7@gmail.com
 * Company      TRUSTUDIO
 */
public class EventPelatihan {
    public int id;
    public String title;
    public String category;
    public String description;
    public String price;
    public String available;
    public String registered;
    public String province_name;
    public String city_name;
    public String address;
    public String created_at;
    public String updated_at;
    public String place;
    public String creator;
    public String poster_file_name;
    public String category_program;

    public String latitude;
    public String longitude;
    public String logo;
    public int comments;

    public ArrayList<EventSchedule> listSchedule    = new ArrayList<>();
    public ArrayList<EventFacilitys> listFacilitys  = new ArrayList<>();
    public ArrayList<EventTopics> listTopics        = new ArrayList<>();

}

package com.improve.app.model;

/**
 * Created by   yudi rahmat
 * Email        yudirahmat7@gmail.com
 */
public class EventSchedule {
    public String description;
    public String date_start;
    public String date_end;
}

package com.improve.app.model;

/**
 * Created by Yudi on 2/14/2016.
 */
public class CartModel {
    public int id;
    public String nama_pemesan;
    public String email_pemesan;
    public int total;
    public String state;
    public String item_count;
    public int program_id;
    public String category;
    public String title;
}

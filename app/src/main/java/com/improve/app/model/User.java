package com.improve.app.model;

/**
 * Created by   yudi rahmat
 * Email        yudirahmat7@gmail.com
 * Company      TRUSTUDIO
 */
public class User {
    public int id;
    public String email;
    public String created_at;
    public String updated_at;
    public String role_id;
    public String role_name;
    public String username;
    public String birthday;
    public String phone;
    public String address;
    public String province;
    public String city;
    public String photo;
}

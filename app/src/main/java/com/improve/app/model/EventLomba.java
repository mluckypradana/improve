package com.improve.app.model;

import java.util.ArrayList;

/**
 * Created by   yudi rahmat
 * Email        yudirahmat7@gmail.com
 */
public class EventLomba {
    public int id;
    public String title;
    public String description;
    public String price;
    public String available;
    public String registered;
    public String city;
    public String address;
    public String created_at;
    public String updated_at;
    public String category;
    public String creator;
    public String poster_file_name;
    public int comments;
    public String logo;
    public String category_program;

    public ArrayList<EventSchedule> listSchedule = new ArrayList<>();
}

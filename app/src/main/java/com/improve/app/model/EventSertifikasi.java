package com.improve.app.model;

import java.util.ArrayList;

/**
 * Created by   yudi rahmat
 * Email        yudirahmat7@gmail.com
 */
public class EventSertifikasi {
    public int id;
    public String category;
    public String title;
    public String price;
    public String available;
    public String registered;
    public String province_name;
    public String city_name;
    public String address;
    public String created_at;
    public String updated_at;
    public String creator;
    public String poster;
    public String description;
    public String category_program;

    public String latitude;
    public String longitude;
    public int comments;
    public String logo;

    public ArrayList<String> listPrograms               = new ArrayList<>();
    public ArrayList<EventSchedule> listSchedule        = new ArrayList<>();
    public ArrayList<String> listRequirements           = new ArrayList<>();
    public ArrayList<EventFacilitys> listFacilities     = new ArrayList<>();

}

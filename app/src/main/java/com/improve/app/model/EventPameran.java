package com.improve.app.model;

import java.util.ArrayList;

/**
 * Created by maya on 11/28/2015.
 * Created by   yudi rahmat
 * Email        yudirahmat7@gmail.com
 * Company      TRUSTUDIO
 */
public class EventPameran {
    public int id;
    public String category;
    public String title;
    public String title_description;

    public String place;
    public String price;
    public String available;
    public String registered;
    public String province_name;
    public String city_name;
    public String address;
    public String created_at;
    public String updated_at;
    public String creator;
    public String poster;
    public String category_program;

    public String latitude;
    public String longitude;
    public int comments;
    public String logo;

    public ArrayList<EventDates> listDate           = new ArrayList<>();
    public ArrayList<EventSchedule> listSchedule    = new ArrayList<>();

}

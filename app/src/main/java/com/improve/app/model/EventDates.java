package com.improve.app.model;

/**
 * Created by maya on 11/28/2015.
 * Created by   yudi rahmat
 * Email        yudirahmat7@gmail.com
 * Company      TRUSTUDIO
 */
public class EventDates {
    public String date_start;
    public String date_end;

    public String getDate_start() {
        return date_start;
    }

    public void setDate_start(String date_start) {
        this.date_start = date_start;
    }

    public String getDate_end() {
        return date_end;
    }

    public void setDate_end(String date_end) {
        this.date_end = date_end;
    }
}

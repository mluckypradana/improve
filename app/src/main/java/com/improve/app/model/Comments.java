package com.improve.app.model;

/**
 * Created by   yudi rahmat
 * Email        yudirahmat7@gmail.com
 */
public class Comments {
    public int user_id;
    public String username;
    public String photo;
    public String comment;
    public String created_at;
}

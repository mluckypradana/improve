package com.improve.app.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.improve.app.R;
import com.improve.app.interfaceview.IFilterView;
import com.improve.app.utils.Cons;
import com.improve.app.utils.FontUtil;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by   yudi rahmat
 * Email        yudirahmat7@gmail.com
 */

public class BaseActivity extends AppCompatActivity {
    private IFilterView listenerFilter;

	//protected SQLiteDatabase mSqLite;
	protected SharedPreferences mSharedPref;
/*
    private boolean mIsDbOpen 	= false;
	private boolean mEnableDb 	= false;

	private int DB_VERSION 		= Cons.DB_VERSION;*/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        customizeFont();

        mSharedPref = getSharedPreferences(Cons.PRIVATE_PREF, Context.MODE_PRIVATE);

		/*
		init();
		enableDatabase();*/

        setStatusBarColor();

        if (android.os.Build.VERSION.SDK_INT >= 21) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.primary_dark));
        }

    }

    /**
     * Customize all application font
     */
    private void customizeFont() {
        FontUtil.setDefaultFont(this, "DEFAULT", Cons.DEFAULT_FONT);
        FontUtil.setDefaultFont(this, "MONOSPACE", Cons.DEFAULT_FONT);
        FontUtil.setDefaultFont(this, "SANS_SERIF", Cons.DEFAULT_FONT);
    }

    public void setStatusBarColor() {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.primary_dark));
        }
    }

	public void setStatusBarColor(int color){
		if (android.os.Build.VERSION.SDK_INT >= 21){
			Window window = this.getWindow();
			window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
			window.setStatusBarColor(this.getResources().getColor(color));
		}
	}

	public void showDialogConfirmExit() {
		new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
				.setTitleText(this.getResources().getString(R.string.label_dialog_exit_title))
				.setContentText(this.getResources().getString(R.string.label_dialog_exit_message))
				.setCancelText(this.getResources().getString(R.string.label_dialog_no))
				.setConfirmText(this.getResources().getString(R.string.label_dialog_yes))
				.showCancelButton(true)
				.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
					@Override
					public void onClick(SweetAlertDialog sDialog) {
						sDialog.dismiss();

					}
				})
				.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
					@Override
					public void onClick(SweetAlertDialog sDialog) {
						finish();
					}
				})
				.show();
	}

	public void showDialogSuccess(String title, String message) {
		new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE)
				.setTitleText(title)
				.setContentText(message)
				.show();
	}

	public void showDialogFailed(String title, String message) {
		new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
				.setTitleText(title)
				.setContentText(message)
				.show();
	}

	/*private void init() {

		String db			= Cons.DBPATH + Cons.DBNAME;

		int currVersion		= mSharedPref.getInt(Cons.DBVER_KEY, 1);
		DB_VERSION 			= mSharedPref.getInt(Cons.DB_VERSION_NAME, Cons.DB_VERSION);

		try {
			Debug.i(Cons.TAG, "Current database version is " + String.valueOf(currVersion));

			if (DB_VERSION > currVersion) {
				File dbFile  = new File(db);

				if (dbFile.exists()) {
					Debug.i(Cons.TAG, "Deleting current database " + Cons.DBNAME);

					dbFile.delete();
				}

				InputStream is	= getResources().getAssets().open(Cons.DBNAME);
				OutputStream os = new FileOutputStream(db);

				byte[] buffer	= new byte[1024];
				int length;

				Debug.i(Cons.TAG, "Start copying new database " + db + " version " + DB_VERSION);

				while ((length = is.read(buffer)) > 0) {
					os.write(buffer, 0, length);
				}

				os.flush();
				os.close();
				is.close();

				Editor editor = mSharedPref.edit();

				editor.putInt(Cons.DBVER_KEY, DB_VERSION);

				editor.commit();
			}
		} catch (SecurityException ex) {
			Debug.e(Cons.TAG, "Failed to delete current database " + Cons.DBNAME, ex);
		} catch (IOException ex) {
			Debug.e(Cons.TAG, "Failed to copy new database " + Cons.DBNAME + " version " + String.valueOf(DB_VERSION), ex);
		}

	}

	@Override
	protected void onResume() {
		super.onResume();

		if (mEnableDb && !mIsDbOpen) {
			openDatabase();
		}
	}

	@Override
	protected void onPause() {
		if (mEnableDb && mIsDbOpen) {
			closeDatabase();
		}

		super.onPause();
	}

	public SQLiteDatabase getDatabase() {
    	return mSqLite;
    }*/

    public SharedPreferences getSharedPreferences() {
        return mSharedPref;
    }
/*
	public void enableDatabase() {
		mEnableDb = true;

		openDatabase();
	}

	private void openDatabase() {
		if (mIsDbOpen) {
			Debug.i(Cons.TAG, "Database already open");
			return;
		}

		String db = Cons.DBPATH + Cons.DBNAME;

		try {
			mSqLite 	= SQLiteDatabase.openDatabase(db, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
			mIsDbOpen	= mSqLite.isOpen();

			Debug.i(Cons.TAG, "Database open");
		} catch (SQLiteException e) {
			Debug.e(Cons.TAG, "Can not open database " + db, e);
		}
	}

	private void closeDatabase() {
		if (!mIsDbOpen) return;

		mSqLite.close();

		mIsDbOpen = false;

		Debug.i(Cons.TAG, "Database closed");
	}*/

    public void ShowToast(String text) {
        Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
    }

    public void showDialogMessage(String title, String message) {
        final AlertDialog alertDialog;
        alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						alertDialog.dismiss();
					}
				});
        alertDialog.show();
    }

    public void startNewActivity(Class<?> cls) {
        Intent intent = new Intent(this, cls);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    public void startNewActivity2(Class<?> cls) {
        Intent intent = new Intent(this, cls);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public void setSpinnerAdapter(Spinner spinners, String[] list) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.item_list_spinner, list);
        adapter.setDropDownViewResource(R.layout.item_list_spinner_dropdown);
        spinners.setAdapter(adapter);
    }

    public String getLatitude() {
        return getSharedPreferences().getString(Cons.PACKAGE_PREFIX + "latitude", "");
    }

    public String getLongitude() {
        return getSharedPreferences().getString(Cons.PACKAGE_PREFIX + "longitude", "");
    }

    public int getUserId() {
        return mSharedPref.getInt(Cons.PACKAGE_PREFIX + "user_id", 0);
    }

    public void clearLoginData(SharedPreferences.Editor editor) {
        String data = null;

        editor.putInt(Cons.PACKAGE_PREFIX + "user_id", 0);
        editor.putString(Cons.PACKAGE_PREFIX + "user_email", data);
        editor.putString(Cons.PACKAGE_PREFIX + "user_created_at", data);
        editor.putString(Cons.PACKAGE_PREFIX + "user_updated_at", data);
        editor.putString(Cons.PACKAGE_PREFIX + "user_role_id", data);
        editor.putString(Cons.PACKAGE_PREFIX + "user_role_name", data);
        editor.putString(Cons.PACKAGE_PREFIX + "user_username", data);
        editor.putString(Cons.PACKAGE_PREFIX + "user_birthday", data);
        editor.putString(Cons.PACKAGE_PREFIX + "user_phone", data);
        editor.putString(Cons.PACKAGE_PREFIX + "user_address", data);
        editor.putString(Cons.PACKAGE_PREFIX + "user_province", data);
        editor.putString(Cons.PACKAGE_PREFIX + "user_city", data);
        editor.putString(Cons.PACKAGE_PREFIX + "user_photo", data);
        editor.commit();
    }
	/*public SharedPreferences getPrivatePreferences() {
		return mSharedPref;
	}

	public String getAccessToken() {
		return mSharedPref.getString(Cons.ACCESSTOKEN_KEY, "");
	}*/


    public IFilterView getListenerFilter() {
        return listenerFilter;
    }

    public void setListenerFilter(IFilterView listenerFilter) {
        this.listenerFilter = listenerFilter;
    }

    @SuppressWarnings({ "deprecation", "unused" })
    public String getImagePath(Uri imageUri) {
        String imgPath = "";
        Cursor cursor = null;

        try {
            String[] proj = { MediaStore.Images.Media.DATA,
                    MediaStore.Images.Media._ID,
                    MediaStore.Images.Thumbnails._ID,
                    MediaStore.Images.ImageColumns.ORIENTATION };

            cursor = getContentResolver().query(imageUri, proj, null, null, null);
            int columnIndex = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media._ID);
            int columnIndexThumb = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Thumbnails._ID);
            int file_ColumnIndex = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);

            int size = cursor.getCount();

            if (size > 0) {

                int thumbID = 0;
                if (cursor.moveToFirst()) {
                    int imageID = cursor.getInt(columnIndex);

                    thumbID = cursor.getInt(columnIndexThumb);

                    String Path = cursor.getString(file_ColumnIndex);

                    imgPath = Path;
                }

            }

            cursor.close();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        cursor.close();

        return imgPath;
    }
}

package com.improve.app.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.improve.app.R;
import com.improve.app.fragments.ProfileFragment;

/**
 * Created by   yudi rahmat
 * Email        yudirahmat7@gmail.com
 */
public class ProfileActivity extends BaseActivity {
    private Toolbar toolbar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_detail);

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        View v = findViewById(R.id.frame);
        if (v == null) {
        } else {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            ProfileFragment mFragment = new ProfileFragment();
            fragmentTransaction.add(R.id.frame, mFragment);
            fragmentTransaction.commit();
        }
        setupActionBar();
    }

    private void setupActionBar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.icon_left);
        getSupportActionBar().setTitle("PROFIL");

        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        //toolbar.setLogo(R.drawable.icon_actionbar_logo);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            //getSupportActionBar().setTitle(extras.getString(Cons.PACKAGE_PREFIX + "name"));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}

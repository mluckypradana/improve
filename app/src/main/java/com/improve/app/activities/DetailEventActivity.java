package com.improve.app.activities;

import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.improve.app.R;

/**
 * Created by   yudi rahmat
 * Email        yudirahmat7@gmail.com
 * Company      TRUSTUDIO
 */
public class DetailEventActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_event_detail);

        if (android.os.Build.VERSION.SDK_INT >= 21){
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.primary_dark));
        }
    }
}

package com.improve.app.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.improve.app.R;
import com.improve.app.fragments.ChooseRequestEventFragment;
import com.improve.app.fragments.LombaFragment;
import com.improve.app.fragments.PameranFragment;
import com.improve.app.fragments.PelatihanFragment;
import com.improve.app.fragments.RequestEventFragment;
import com.improve.app.fragments.SeminarFragment;
import com.improve.app.fragments.SertifikasiFragment;
import com.improve.app.http.CityConnection;
import com.improve.app.interfaceview.IFilterView;
import com.improve.app.model.CitiesModel;
import com.improve.app.utils.Cons;
import com.improve.app.utils.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yudi on 2/14/2016.
 */
public class HomeNewActivity extends BaseActivity implements View.OnClickListener, CityConnection.CitiesConnectionListener {
    private CityConnection mCityConnection;
    private IFilterView listenerFilter;
    
    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor editor;

    private ViewPager viewPager;
    private Toolbar toolbar;

    private LinearLayout llFilter;
    private Spinner spProvince, spCity;
    private TextView tvSearchByCity, tvSearchByCategory, tvSearchByTitle;
    private ImageView ivHome, ivProfile, ivSearch, ivNotification, ivMoreMenu;
    private ImageView ivSeminar, ivLomba, ivSertifikasi, ivPameran, ivPelatihan, ivRequestEvent, ivMenuMoreToolbar;

    private List<CitiesModel> listProvince;
    private List<CitiesModel> listCities;

    private boolean moreMenu    = false;
    private int pagePosition    = 0;

    private int FILTER_BY_CATEGORY  = 1;
    private int FILTER_BY_TITLE     = 2;
    private int FILTER_BY_CITY      = 3;

    /*public interface FilterConnectionListener {
        void filterResponse(String type, String searchTitle, int searchCityId, String searchCategory);
    }*/

    public HomeNewActivity() { }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_new);

        mSharedPreferences  = getSharedPreferences(Cons.PRIVATE_PREF, Context.MODE_PRIVATE);
        editor              = mSharedPreferences.edit();

        toolbar             = (Toolbar) findViewById(R.id.toolbar);
        viewPager           = (ViewPager) findViewById(R.id.viewpager);
        llFilter            = (LinearLayout) findViewById(R.id.ll_filter);

        ivHome			    = (ImageView) findViewById(R.id.iv_footer_menu_home);
        ivProfile		    = (ImageView) findViewById(R.id.iv_footer_menu_profile);
        ivSearch		    = (ImageView) findViewById(R.id.iv_footer_menu_search);
        ivNotification	    = (ImageView) findViewById(R.id.iv_footer_menu_notication);
        ivMoreMenu		    = (ImageView) findViewById(R.id.iv_footer_menu_more);

        ivSeminar           = (ImageView) findViewById(R.id.iv_menu_seminar);
        ivLomba             = (ImageView) findViewById(R.id.iv_menu_lomba);
        ivSertifikasi       = (ImageView) findViewById(R.id.iv_menu_sertifikasi);
        ivPameran           = (ImageView) findViewById(R.id.iv_menu_pameran);
        ivPelatihan         = (ImageView) findViewById(R.id.iv_menu_pelatihan);
        ivRequestEvent      = (ImageView) findViewById(R.id.iv_menu_request_event);
        ivMenuMoreToolbar   = (ImageView) findViewById(R.id.iv_toolbar_more_menu);

        tvSearchByCity      = (TextView) findViewById(R.id.btn_search_city);
        tvSearchByCategory  = (TextView) findViewById(R.id.btn_search_category);
        tvSearchByTitle     = (TextView) findViewById(R.id.btn_search_title);

        mCityConnection     = new CityConnection(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setElevation(0);
        getSupportActionBar().setTitle("");

        toolbar.setTitle("");
        toolbar.setBackgroundColor(getResources().getColor(R.color.toolbar_color));

        if (viewPager != null) {
            setupViewPager(viewPager);
        }

        showMenuSelected(0);

        ivHome.setOnClickListener(this);
        ivProfile.setOnClickListener(this);
        ivSearch.setOnClickListener(this);
        ivNotification.setOnClickListener(this);
        ivMoreMenu.setOnClickListener(this);

        ivSeminar.setOnClickListener(this);
        ivLomba.setOnClickListener(this);
        ivSertifikasi.setOnClickListener(this);
        ivPameran.setOnClickListener(this);
        ivPelatihan.setOnClickListener(this);
        ivRequestEvent.setOnClickListener(this);
        ivMenuMoreToolbar.setOnClickListener(this);

        tvSearchByCity.setOnClickListener(this);
        tvSearchByCategory.setOnClickListener(this);
        tvSearchByTitle.setOnClickListener(this);

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) { }

            @Override
            public void onPageSelected(int position) {
                showMenuSelected(position);
                pagePosition    = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) { }
        });

    }

    private void setupViewPager(final ViewPager viewPager) {
        Adapter adapter = new Adapter(getSupportFragmentManager());
        adapter.addFragment(new SeminarFragment(),              "Seminar");
        adapter.addFragment(new LombaFragment(),                "Lomba");
        adapter.addFragment(new SertifikasiFragment(),          "Sertifikasi");
        adapter.addFragment(new PameranFragment(),              "Pameran");
        adapter.addFragment(new PelatihanFragment(),            "Pelatihan");
        adapter.addFragment(new ChooseRequestEventFragment(),   "Request Event");
        viewPager.setAdapter(adapter);

    }

    public String[] getListSpinner(List<CitiesModel> mList) {
        String[] list;

        if(mList != null && mList.size() > 0) {
            list  = new String[mList.size()];

            for (int i=0; i<mList.size(); i++) {
                list[i] = mList.get(i).name;
            }
        } else {
            list = new String[]{"null"};
        }

        return list;
    }

    @Override
    public void EventsConnection(int status, String message, ArrayList<CitiesModel> mCities) {
        if (status == 1) {
            if (mCities != null && mCities.size() > 0) {
                int category = mCities.get(0).category;

                if (category == 0) {
                    listProvince = mCities;
                    setSpinnerAdapter(spProvince, getListSpinner(mCities));

                } else {
                    listCities = mCities;
                    setSpinnerAdapter(spCity, getListSpinner(mCities));
                }
            }
        }
    }

    static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragments = new ArrayList<>();
        private final List<String> mFragmentTitles = new ArrayList<>();

        public Adapter(FragmentManager fm) {
            super(fm);
        }

        public void addFragment(Fragment fragment, String title) {
            mFragments.add(fragment);
            mFragmentTitles.add(title);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitles.get(position);
        }
    }

    private void showMenuSelected(int position) {
        ivSeminar.setImageResource(R.drawable.icon_menu_event_normal);
        ivLomba.setImageResource(R.drawable.icon_menu_lomba_normal);
        ivSertifikasi.setImageResource(R.drawable.icon_menu_sertificate_normal);
        ivPameran.setImageResource(R.drawable.icon_menu_pameran_normal);
        ivPelatihan.setImageResource(R.drawable.icon_menu_pelatihan_normal);
        ivRequestEvent.setImageResource(R.drawable.icon_menu_request_normal);

        if(position == 0)
            ivSeminar.setImageResource(R.drawable.icon_menu_event_pressed);
        else if(position == 1)
            ivLomba.setImageResource(R.drawable.icon_menu_lomba_pressed);
        else if(position == 2)
            ivSertifikasi.setImageResource(R.drawable.icon_menu_sertificate_pressed);
        else if(position == 3)
            ivPameran.setImageResource(R.drawable.icon_menu_pameran_pressed);
        else if(position == 4)
            ivPelatihan.setImageResource(R.drawable.icon_menu_pelatihan_pressed);
        else if(position == 5)
            ivRequestEvent.setImageResource(R.drawable.icon_menu_request_pressed);
    }

    private void showFilterData(final int type) {
        listenerFilter                  = getListenerFilter();
        AlertDialog.Builder builder     = new AlertDialog.Builder(this);
        LayoutInflater inflater         = LayoutInflater.from(this);

        View view = inflater.inflate(R.layout.layout_dialog_filter_event, null);

        LinearLayout llCategory         = (LinearLayout) view.findViewById(R.id.ll_category);
        LinearLayout llCity             = (LinearLayout) view.findViewById(R.id.ll_city);
        final EditText etName           = (EditText) view.findViewById(R.id.et_name);
        final Spinner spCategory        = (Spinner) view.findViewById(R.id.sp_category);
        spCity                          = (Spinner) view.findViewById(R.id.sp_city);
        spProvince                      = (Spinner) view.findViewById(R.id.sp_province);

        if(type == FILTER_BY_CATEGORY) {
            llCategory.setVisibility(View.VISIBLE);
            llCity.setVisibility(View.GONE);
            etName.setVisibility(View.GONE);

        } else if(type == FILTER_BY_CITY) {
            llCategory.setVisibility(View.GONE);
            llCity.setVisibility(View.VISIBLE);
            etName.setVisibility(View.GONE);

        } else if(type == FILTER_BY_TITLE) {
            llCategory.setVisibility(View.GONE);
            llCity.setVisibility(View.GONE);
            etName.setVisibility(View.VISIBLE);

        }

        showCities();
        setSpinnerAdapter(spCategory, getResources().getStringArray(R.array.arr_category));

        builder.setView(view)
                .setPositiveButton(getResources().getString(R.string.label_dialog_search), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        String name = etName.getText().toString();

                        if(type == FILTER_BY_CATEGORY) {
                            listenerFilter.filterResponse(Util.categoryFilter[pagePosition], null, 0, spCategory.getSelectedItem().toString().toLowerCase());

                        } else if(type == FILTER_BY_CITY) {
                            if(listCities != null && listCities.size() > 0) {
                                int city = listCities.get(spCity.getSelectedItemPosition()).id;
                                listenerFilter.filterResponse(Util.categoryFilter[pagePosition], null, city, null);

                            } else {
                                ShowToast("Kota Tidak Tersedia !");
                            }

                        } else if(type == FILTER_BY_TITLE) {
                            if (name == null || name.equals("")) {
                                ShowToast("Data Tidak Boleh Kosong !");
                            } else {
                                listenerFilter.filterResponse(Util.categoryFilter[pagePosition], name, 0, null);
                            }
                        }

                    }
                })
                .setNegativeButton(getResources().getString(R.string.label_dialog_cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        builder.create();
        builder.show();
    }

    private void showCities() {
        mCityConnection.getCities(0);

        spProvince.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //connectionStatus    = mConnection.isConnectingToInternet();

                setSpinnerAdapter(spCity,  new String[] {"Load Data ..."});

                if(listProvince != null && listProvince.size() > 0)
                    mCityConnection.getCities(listProvince.get(position).id);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) { }
        });
    }

    @Override
    public void onClick(View v) {
        int Id = v.getId();

        if(Id == R.id.iv_footer_menu_home) {
            startNewActivity2(HomeNewActivity.class);

        } else if(Id == R.id.iv_footer_menu_profile) {
            if (Util.getUserId(this) == null || Util.getUserId(this).equals("") || getUserId() == 0) {
                ShowToast("Silahkan Login Dahulu");
                startNewActivity2(LoginActivity.class);

            } else
                startNewActivity2(ProfileListActivity.class);

        } else if(Id == R.id.iv_footer_menu_search) {
            ShowToast("Menu Belum Tersedia");

        } else if(Id == R.id.iv_footer_menu_notication) {
            ShowToast("Menu Belum Tersedia");

        } else if(Id == R.id.iv_footer_menu_more) {
            startNewActivity2(MoreMenuActivity.class);

        } else if(Id == R.id.iv_menu_seminar) {
            viewPager.setCurrentItem(0);

        } else if(Id == R.id.iv_menu_lomba) {
            viewPager.setCurrentItem(1);

        } else if(Id == R.id.iv_menu_sertifikasi) {
            viewPager.setCurrentItem(2);

        } else if(Id == R.id.iv_menu_pameran) {
            viewPager.setCurrentItem(3);

        } else if(Id == R.id.iv_menu_pelatihan) {
            viewPager.setCurrentItem(4);

        } else if(Id == R.id.iv_menu_request_event) {
            viewPager.setCurrentItem(5);

        } else if(Id == R.id.iv_toolbar_more_menu) {
            if(pagePosition < 5) {
                if(moreMenu == false) {
                    moreMenu = true;

                    toolbar.setBackgroundColor(getResources().getColor(R.color.orange));
                    ivMenuMoreToolbar.setImageResource(R.drawable.icon_menu_more_pressed);
                    llFilter.setVisibility(View.VISIBLE);

                } else {
                    moreMenu = false;

                    toolbar.setBackgroundColor(getResources().getColor(R.color.toolbar_color));
                    ivMenuMoreToolbar.setImageResource(R.drawable.icon_menu_more);
                    llFilter.setVisibility(View.GONE);
                }
            }
        } else if (Id == R.id.btn_search_city) {
            if(pagePosition < 5)
                showFilterData(FILTER_BY_CITY);
        } else if (Id == R.id.btn_search_category) {
            if(pagePosition < 5)
                showFilterData(FILTER_BY_CATEGORY);
        } else if (Id == R.id.btn_search_title) {
            if(pagePosition < 5)
                showFilterData(FILTER_BY_TITLE);
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {

            showDialogConfirmExit();

            onKeyDown(DEFAULT_KEYS_DISABLE, null);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void setListenerFilter2(IFilterView listenerFilter) {
        this.listenerFilter = listenerFilter;
    }
}

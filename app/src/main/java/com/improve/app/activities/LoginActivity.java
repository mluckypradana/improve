package com.improve.app.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.improve.app.R;
import com.improve.app.http.LoginConnection;
import com.improve.app.model.User;
import com.improve.app.utils.Cons;
import com.improve.app.utils.Util;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by   yudi rahmat
 * Email        yudirahmat7@gmail.com
 */
public class LoginActivity extends BaseActivity implements LoginConnection.LoginConnectionListener {
    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor editor;

    private LoginConnection mConnection;

    private SweetAlertDialog progressDialog;
    private ProgressDialog mProgressDialog;
    private boolean mIsLoading = false;

    private TextView tvSkip;
    private EditText tvUsername, tvPassword;
    private Button btnLogin, btnRegister;

    private boolean checkForm = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mSharedPreferences  = getSharedPreferences(Cons.PRIVATE_PREF, Context.MODE_PRIVATE);
        editor              = mSharedPreferences.edit();

        tvUsername          = (EditText) findViewById(R.id.username);
        tvPassword          = (EditText) findViewById(R.id.password);
        tvSkip              = (TextView) findViewById(R.id.tv_label_skip);
        btnLogin            = (Button) findViewById(R.id.btn_login);
        btnRegister         = (Button) findViewById(R.id.btn_register);

        mConnection         = new LoginConnection(this);
        mProgressDialog     = new ProgressDialog(this);
        progressDialog      = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);

        btnLogin.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                login();
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                startNewActivity2(RegisterActivity.class);
            }
        });

        tvSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startNewActivity(HomeNewActivity.class);
            }
        });

        if (Cons.ENABLE_DEVELOP == true) {
            tvUsername.setText("superadmin@improve.com");
            tvPassword.setText("12345678");
        }

        autoLogin();

    }

    private void autoLogin() {
        if (Util.getUserId(this) == null || Util.getUserId(this).equals("") || getUserId() == 0) {

        } else {
            startNewActivity(HomeNewActivity.class);
        }
    }

    private void login() {
        checkForm           = true;

        String username     = tvUsername.getText().toString();
        String password     = tvPassword.getText().toString();

        if (username.isEmpty()) {
            tvUsername.setError(getResources().getString(R.string.label_error_hint));

            checkForm = false;
        }

        if (password.isEmpty()) {
            tvPassword.setError(getResources().getString(R.string.label_error_hint));

            checkForm = false;
        }

        if(checkForm == true) {
            try {
                showProgressDialog();

                mConnection.login(username, password);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void showProgressDialog() {
        mIsLoading = true;

        progressDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.orange));
        progressDialog.setTitleText("Please Wait ...");
        progressDialog.show();

        /*mProgressDialog.setMessage("Please Wait...");
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();*/

        btnLogin.setEnabled(false);
    }

    private void dismissProgressDialog() {
        mIsLoading = false;

        mProgressDialog.dismiss();
        progressDialog.dismiss();

        btnLogin.setEnabled(true);
    }

    /**
     * Save login data to preference
     *
     * @param mLogin
     */
    private void setLoginData(ArrayList<User> mLogin) {
        if(mLogin != null && mLogin.size() > 0) {
            User users     = mLogin.get(0) ;
            Util.saveString(this,Cons.USER_ID,users.id+"");
            editor.putInt(Cons.PACKAGE_PREFIX + "user_id",                      users.id);
            editor.putString(Cons.PACKAGE_PREFIX + "user_password",             tvPassword.getText().toString());
            editor.putString(Cons.PACKAGE_PREFIX + "user_email",                users.email);
            editor.putString(Cons.PACKAGE_PREFIX + "user_created_at",           users.created_at);
            editor.putString(Cons.PACKAGE_PREFIX + "user_updated_at",           users.updated_at);
            editor.putString(Cons.PACKAGE_PREFIX + "user_role_id",              users.role_id);
            editor.putString(Cons.PACKAGE_PREFIX + "user_role_name",            users.role_name);
            editor.putString(Cons.PACKAGE_PREFIX + "user_username",             users.username);
            editor.putString(Cons.PACKAGE_PREFIX + "user_birthday",             users.birthday);
            editor.putString(Cons.PACKAGE_PREFIX + "user_phone",                users.phone);
            editor.putString(Cons.PACKAGE_PREFIX + "user_address",              users.address);
            editor.putString(Cons.PACKAGE_PREFIX + "user_province",             users.province);
            editor.putString(Cons.PACKAGE_PREFIX + "user_city",                 users.city);
            editor.putString(Cons.PACKAGE_PREFIX + "user_photo",                users.photo);
            editor.commit();
        }

    }

    @Override
    public void LoginConnection(int status, String message, ArrayList<User> mLogin) {
        dismissProgressDialog();

        if (status == 1 && mLogin != null && mLogin.size() > 0) {
            setLoginData(mLogin);

            startNewActivity(HomeNewActivity.class);
        } else if (status == 0) {
            showDialogMessage("Error", message);
        } else {
            showDialogMessage("Error", message);
        }

    }
}

package com.improve.app.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.improve.app.R;
import com.improve.app.fragments.DetailLombaFragment;


/**
 * Created by   Rian Erlangga Saputra
 * Email        rianerlangga03@gmail.com
 */
public class DetailLombaActivity extends BaseActivity {
    private Toolbar toolbar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_detail);

        toolbar                 = (Toolbar) findViewById(R.id.toolbar);

        View v = findViewById(R.id.frame);
        if (v == null) {
        } else {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            DetailLombaFragment mFragment	= new DetailLombaFragment();
            fragmentTransaction.add(R.id.frame, mFragment);
            fragmentTransaction.commit();
        }
        setupActionBar();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupActionBar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.icon_left);
        getSupportActionBar().setTitle("");

        toolbar.setTitleTextColor(getResources().getColor(R.color.white));

        //toolbar.setLogo(R.drawable.icon_actionbar_logo);

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            //getSupportActionBar().setTitle(extras.getString(Cons.PACKAGE_PREFIX + "name"));
        }
    }
}

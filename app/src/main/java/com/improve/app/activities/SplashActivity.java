package com.improve.app.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.improve.app.R;
import com.improve.app.utils.Util;
import com.improve.app.widget.RotateLoading;

import java.util.Timer;
import java.util.TimerTask;

public class SplashActivity extends BaseActivity {
    private RotateLoading mRotateLoading;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        mRotateLoading          = (RotateLoading) findViewById(R.id.rotateloading);

        mRotateLoading.start();

        setStatusBarColor(R.color.blueLight3);
        Util.createAppDir("Data/Profil/Photo");

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                showLoginPage();
            }

        }, 3000);
    }

    private void showLoginPage() {
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }
}

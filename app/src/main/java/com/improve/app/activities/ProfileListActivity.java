package com.improve.app.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.improve.app.R;
import com.improve.app.fragments.CartListFragment;
import com.improve.app.fragments.ProfileMenuFragment;
import com.improve.app.utils.Util;

/**
 * Created by   yudi rahmat
 * Email        yudirahmat7@gmail.com
 */
public class ProfileListActivity extends BaseActivity implements View.OnClickListener {
    private Toolbar toolbar;

    private LinearLayout llFooterMenu;
    private ImageView ivHome, ivProfile, ivSearch, ivNotification, ivMoreMenu;
    private View vLine1, vLine2;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_detail);

        toolbar             = (Toolbar) findViewById(R.id.toolbar);

        ivHome			    = (ImageView) findViewById(R.id.iv_footer_menu_home);
        ivProfile		    = (ImageView) findViewById(R.id.iv_footer_menu_profile);
        ivSearch		    = (ImageView) findViewById(R.id.iv_footer_menu_search);
        ivNotification	    = (ImageView) findViewById(R.id.iv_footer_menu_notication);
        ivMoreMenu		    = (ImageView) findViewById(R.id.iv_footer_menu_more);

        llFooterMenu        = (LinearLayout) findViewById(R.id.ll_footer_menu);

        vLine1              = (View) findViewById(R.id.v_line_1);
        vLine2              = (View) findViewById(R.id.v_line_2);

        View v = findViewById(R.id.frame);
        if (v == null) {
        } else {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            ProfileMenuFragment mFragment = new ProfileMenuFragment();
            fragmentTransaction.add(R.id.frame, mFragment);
            fragmentTransaction.commit();
        }
        setupActionBar();

        ivHome.setOnClickListener(this);
        ivProfile.setOnClickListener(this);
        ivSearch.setOnClickListener(this);
        ivNotification.setOnClickListener(this);
        ivMoreMenu.setOnClickListener(this);

        llFooterMenu.setVisibility(View.VISIBLE);
        vLine1.setVisibility(View.GONE);
        vLine2.setVisibility(View.VISIBLE);
    }

    private void setupActionBar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.icon_left);
        getSupportActionBar().setTitle("Profile");

        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        //toolbar.setLogo(R.drawable.icon_actionbar_logo);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            //getSupportActionBar().setTitle(extras.getString(Cons.PACKAGE_PREFIX + "name"));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        int Id = v.getId();

        if(Id == R.id.iv_footer_menu_home) {
            startNewActivity2(HomeNewActivity.class);

        } else if(Id == R.id.iv_footer_menu_profile) {
            if (Util.getUserId(this) == null || Util.getUserId(this).equals("") || getUserId() == 0) {
                ShowToast("Silahkan Login Dahulu");
                startNewActivity2(LoginActivity.class);

            } else
                startNewActivity2(ProfileListActivity.class);

        } else if(Id == R.id.iv_footer_menu_search) {
            ShowToast("Menu Belum Tersedia");

        } else if(Id == R.id.iv_footer_menu_notication) {
            ShowToast("Menu Belum Tersedia");

        } else if(Id == R.id.iv_footer_menu_more) {
            startNewActivity(MoreMenuActivity.class);

        }
    }
}

package com.improve.app.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.widget.TextView;

import com.improve.app.R;
import com.improve.app.fragments.CartListFragment;
import com.improve.app.fragments.LombaFragment;
import com.improve.app.fragments.PameranFragment;
import com.improve.app.fragments.PelatihanFragment;
import com.improve.app.fragments.SeminarFragment;
import com.improve.app.fragments.SertifikasiFragment;
import com.improve.app.utils.Cons;
import com.improve.app.utils.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by   yudi rahmat
 * Email        yudirahmat7@gmail.com
 */
public class HomeActivity extends BaseActivity {
    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor editor;

    private NavigationView navigationView;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private Toolbar toolbar;

    private DrawerLayout mDrawerLayout;
    Context ctx;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        ctx                 = this;
        mSharedPreferences  = getSharedPreferences(Cons.PRIVATE_PREF, Context.MODE_PRIVATE);
        editor              = mSharedPreferences.edit();

        toolbar             = (Toolbar) findViewById(R.id.toolbar);
        viewPager           = (ViewPager) findViewById(R.id.viewpager);
        tabLayout           = (TabLayout) findViewById(R.id.tabs);
        mDrawerLayout       = (DrawerLayout) findViewById(R.id.drawer_layout);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu);
        getSupportActionBar().setElevation(0);
        getSupportActionBar().setTitle("");

        toolbar.setTitle("");
        toolbar.setLogo(R.drawable.icon_actionbar_logo);

        if (viewPager != null) {
            setupViewPager(viewPager);
        }

        tabLayout.setHorizontalScrollBarEnabled(true);
        tabLayout.setSmoothScrollingEnabled(true);
        tabLayout.setupWithViewPager(viewPager);

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        TextView navTitle = (TextView) navigationView.findViewById(R.id.nav_header_text);

        navTitle.setText(mSharedPreferences.getString(Cons.PACKAGE_PREFIX + "user_username", "Anonymous"));

        if (navigationView != null) {
            setupDrawerContent(navigationView);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupViewPager(final ViewPager viewPager) {
        Adapter adapter = new Adapter(getSupportFragmentManager());
        adapter.addFragment(new SeminarFragment(),      "Seminar");
        adapter.addFragment(new PelatihanFragment(),    "Pelatihan");
        adapter.addFragment(new LombaFragment(),        "Lomba");
        adapter.addFragment(new SertifikasiFragment(),  "Sertifikasi");
        adapter.addFragment(new PameranFragment(),      "Pameran");
        viewPager.setAdapter(adapter);

    }

    private void setupDrawerContent(NavigationView mNavigationView) {
        //Check login status
        int id = mSharedPreferences.getInt(Cons.PACKAGE_PREFIX + "user_id", 0);
        //Hide login visibility
        if (id != 0) {
            navigationView.getMenu().getItem(1).setVisible(false);
        } else {
            navigationView.getMenu().getItem(2).setVisible(false);
            navigationView.getMenu().getItem(4).setVisible(false);
        }

        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        menuItem.setChecked(true);
                        mDrawerLayout.closeDrawers();

                        switch (menuItem.getItemId()) {
                            case R.id.nav_seminar:
                                viewPager.setCurrentItem(0);
                                return true;

                            case R.id.nav_pelatihan:
                                viewPager.setCurrentItem(1);
                                return true;

                            case R.id.nav_lomba:
                                viewPager.setCurrentItem(2);
                                return true;

                            case R.id.nav_sertifikasi:
                                viewPager.setCurrentItem(3);
                                return true;

                            case R.id.nav_pameran:
                                viewPager.setCurrentItem(4);
                                return true;

                            case R.id.nav_request_event:
                                startNewActivity2(RequestEventActivity.class);
                                return true;

                            case R.id.nav_login:
                                startNewActivity2(LoginActivity.class);
                                return true;

                            case R.id.nav_register:
                                startNewActivity2(RegisterActivity.class);
                                return true;

                            case R.id.nav_profil:
                                startNewActivity2(ProfileActivity.class);
                                return true;

                            case R.id.nav_cart:
                                startNewActivity2(CartListActivity.class);
                                return true;

                            case R.id.nav_payment:
                                if (Util.getUserId(ctx) == null || Util.getUserId(ctx).equals("") || getUserId() == 0) {

                                    startNewActivity2(LoginActivity.class);
                                } else {
                                    startNewActivity2(ViewConfirmationActivity.class);
                                }
                                return true;

                            case R.id.nav_sertifikat:
                                ShowToast("Menu Belum Tersedia");
                                return true;

                            case R.id.nav_help:
                                ShowToast("Menu Belum Tersedia");
                                return true;

                            case R.id.nav_syarat:
                                ShowToast("Menu Belum Tersedia");
                                return true;

                            case R.id.nav_about:
                                ShowToast("Menu Belum Tersedia");
                                return true;

                            case R.id.nav_logout:
                                clearLoginData(editor);

                                startNewActivity2(HomeActivity.class);
                                return true;
                        }
                        return true;
                    }
                });
    }

    static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragments = new ArrayList<>();
        private final List<String> mFragmentTitles = new ArrayList<>();

        public Adapter(FragmentManager fm) {
            super(fm);
        }

        public void addFragment(Fragment fragment, String title) {
            mFragments.add(fragment);
            mFragmentTitles.add(title);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitles.get(position);
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {

            if (mDrawerLayout.isDrawerOpen(GravityCompat.START) == true)
                mDrawerLayout.closeDrawer(GravityCompat.START);
            else
                showDialogConfirmExit();

            onKeyDown(DEFAULT_KEYS_DISABLE, null);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}

package com.improve.app.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import com.improve.app.R;
import com.improve.app.http.CityConnection;
import com.improve.app.http.DetectConnection;
import com.improve.app.http.RegisterConnection;
import com.improve.app.model.CitiesModel;
import com.improve.app.utils.FileUtils;
import com.improve.app.utils.Util;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by   yudi rahmat
 * Email        yudirahmat7@gmail.com
 */
public class RegisterActivity extends BaseActivity implements RegisterConnection.RegisterConnectionListener, CityConnection.CitiesConnectionListener {
    private RegisterConnection mConnection;
    private CityConnection mCityConnection;
    private DetectConnection connectionStatus;

    private Toolbar toolbar;

    private EditText etUsername, etCity, etEmail, etPassword, etRePassword, etAlamat, etTelp, etDayOfBirth;
    private ImageView ivAddFoto;
    private Spinner spProvince, spCity;

    private List<CitiesModel> listProvince;
    private List<CitiesModel> listCities;

    private SweetAlertDialog progressDialog;
    private ProgressDialog mProgressDialog;
    private boolean mIsLoading  = false;

    private String mBirthday;
    private String imagePath;
    private Uri imgUri;

    private int INTENT_CAMERA = 101;
    private int INTENT_GALLERY = 102;

    private boolean checkForm = false;

    /* Calendar get time */
    Calendar myCal = Calendar.getInstance();

    DatePickerDialog.OnDateSetListener dpl = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            myCal.set(Calendar.YEAR, year);
            myCal.set(Calendar.MONTH, monthOfYear);
            myCal.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            mBirthday = dayOfMonth + "-" + (monthOfYear+1) + "-" + year;
            etDayOfBirth.setText(dayOfMonth + " " + Util.month[monthOfYear] + " " + year);
        }
    };
    /* end Calendar*/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        toolbar                 = (Toolbar) findViewById(R.id.toolbar);
        etUsername              = (EditText) findViewById(R.id.et_username);
        etEmail                 = (EditText) findViewById(R.id.et_email);
        etPassword              = (EditText) findViewById(R.id.et_password);
        etRePassword            = (EditText) findViewById(R.id.et_comfirm_password);
        etCity                  = (EditText) findViewById(R.id.et_city);
        spCity                  = (Spinner) findViewById(R.id.sp_city);
        spProvince              = (Spinner) findViewById(R.id.sp_province);
        etAlamat                = (EditText) findViewById(R.id.et_alamat);
        etTelp                  = (EditText) findViewById(R.id.et_telp);
        etDayOfBirth            = (EditText) findViewById(R.id.et_tanggal_lahir);
        ivAddFoto               = (ImageView) findViewById(R.id.img_add_audit);

        mConnection             = new RegisterConnection(this);
        connectionStatus        = new DetectConnection(this);
        mProgressDialog         = new ProgressDialog(this);
        mCityConnection         = new CityConnection(this);
        progressDialog          = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);

        onClickData();
        setupActionBar();
        showAllList();
        disableSoftInputFromAppearing(etDayOfBirth);
    }

    private void showAllList() {
        mCityConnection.getCities(0);

        spProvince.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //connectionStatus    = mConnection.isConnectingToInternet();

                setSpinnerAdapter(spCity,  new String[] {"Load Data ..."});

                if(listProvince != null && listProvince.size() > 0)
                    mCityConnection.getCities(listProvince.get(position).id);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) { }
        });
    }

    private void setupActionBar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.icon_left);
        getSupportActionBar().setTitle("");
    }

    private void onClickData() {
        ivAddFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    /*Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, 1);*/
                    showChoosePhoto();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        etDayOfBirth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog dpd = new DatePickerDialog(RegisterActivity.this, dpl,
                        myCal.get(Calendar.YEAR),
                        myCal.get(Calendar.MONTH),
                        myCal.get(Calendar.DAY_OF_MONTH));
                dpd.show();
            }
        });
    }

    @SuppressLint("NewApi")
    private void disableSoftInputFromAppearing(final EditText editText) {
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

        if (Build.VERSION.SDK_INT >= 11) {
            editText.setRawInputType(InputType.TYPE_CLASS_TEXT);
            editText.setTextIsSelectable(true);
        } else {
            editText.setRawInputType(InputType.TYPE_NULL);
            editText.setFocusable(true);
        }

        etDayOfBirth.setInputType(0);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        /*if (resultCode == this.RESULT_OK) {
            if(requestCode == 1) {
                Bundle extras           = data.getExtras();
                Bitmap bitMap           = (Bitmap) extras.get("data");
                String[] projection     = { MediaStore.Images.Media.DATA };
                Cursor cursor           = this.managedQuery(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection, null, null, null);
                int column_index_data   = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToLast();

                imagePath               = cursor.getString(column_index_data);
                Bitmap bitmap           = Util.decodeSampledBitmapFromUri(imagePath, 480, 800);

                Util.saveBitmapToFile(imagePath, bitmap);
                ivAddFoto.setImageBitmap(bitMap);
            }
        }*/

        if (resultCode == Activity.RESULT_OK) {
            if(requestCode == INTENT_CAMERA) {
                imagePath = getImagePath(imgUri);

                String fotoPath         = Util.getDir("Data/Profil/Photo/") + Util.getPhotoName(imagePath);

                Bitmap bitmap           = Util.decodeSampledBitmapFromUri(fotoPath, 800, 1280);
                Util.saveBitmapToFile(fotoPath, bitmap);

                imagePath = fotoPath;

                File imgFile = new File(imagePath);
                if(imgFile.exists()){
                    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    ivAddFoto.setImageBitmap(myBitmap);
                }

                ShowToast(imgFile.exists() + "\n" + Util.getFileType(imagePath) + "\n" + imagePath);

            } else if(requestCode == INTENT_GALLERY) {
                String path     = FileUtils.getPath(this, data.getData());

                Bitmap bitmap   = Util.decodeSampledBitmapFromUri(path, 800, 1280);

                String fotoPath = Util.getDir("Data/Profil/Photo/") + Util.getPhotoName(path);

                imagePath       = fotoPath;

                Util.saveBitmapToFile(fotoPath, bitmap);

                File imgFile = new File(fotoPath);
                if(imgFile.exists()){
                    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    ivAddFoto.setImageBitmap(myBitmap);
                }

                ShowToast(imgFile.exists() + "\n" + Util.getFileType(imagePath) + "\n" + imagePath);
            }
        }
    }
    private void showChoosePhoto() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final CharSequence[] array  = {"Take a Photo", "Choose from Gallery"};

        builder
                .setSingleChoiceItems(array, -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(which == 0) {
                            intentToCamera();
                        } else if(which == 1) {
                            intentToGallery();
                        }

                        dialog.dismiss();
                    }
                });

        builder.create();
        builder.show();
    }

    private void intentToGallery() {
        Intent tempIntent = new Intent(Intent.ACTION_GET_CONTENT);
        tempIntent.setType("image/*");
        PackageManager pm = getPackageManager();
        Intent openInChooser = Intent.createChooser(tempIntent, getResources().getString(R.string.select_picture));
        startActivityForResult(openInChooser, INTENT_GALLERY);
    }

    private void intentToCamera() {
        try {
            /*Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, INTENT_CAMERA);*/

            String fileName = System.currentTimeMillis() + ".jpg";
            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.TITLE, fileName);
            values.put(MediaStore.Images.Media.DESCRIPTION,"Image capture by camera");
            imgUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

            Intent intent = new Intent( MediaStore.ACTION_IMAGE_CAPTURE );
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imgUri);
            startActivityForResult(intent, INTENT_CAMERA);
        } catch (Exception e) {
            e.printStackTrace();

            ShowToast(getResources().getString(R.string.label_toast_check_permission_camera));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;

            case R.id.verify:
                checkValidationForm();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void register() {
        try {
            showProgressDialog();

            mConnection.regisetrOrUpdate(false, 0, imagePath, getEmail(), getCity(), getAlamat(), getTelp(), getDayOfBirth(), getUsername(), getPassword());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showProgressDialog() {
        mIsLoading  = true;

        progressDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.orange));
        progressDialog.setTitleText("Please Wait ...");
        progressDialog.show();

        /*mProgressDialog.setMessage("Please Wait...");
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();*/
    }

    private void dismissProgressDialog() {
        mIsLoading  = false;

        mProgressDialog.dismiss();
        progressDialog.dismiss();
    }

    private void checkValidationForm() {
        checkForm = true;

        if (getDayOfBirth().isEmpty() || getDayOfBirth().equals(getResources().getString(R.string.label_error_hint))) {
            etDayOfBirth.setError(getResources().getString(R.string.label_error_hint));

            checkForm = false;
        }

        if (getTelp().isEmpty()) {
            etTelp.setError(getResources().getString(R.string.label_error_hint));

            checkForm = false;
        }

        if (getAlamat().isEmpty()) {
            etAlamat.setError(getResources().getString(R.string.label_error_hint));

            checkForm = false;
        }

        if (getEmail().isEmpty()) {
            etEmail.setError(getResources().getString(R.string.label_error_hint));

            checkForm = false;
        }

        if (getUsername().isEmpty()) {
            etUsername.setError(getResources().getString(R.string.label_error_hint));

            checkForm = false;
        }

        if(getPassword().length() < 8) {
            etPassword.setError(getResources().getString(R.string.label_error_password));

            checkForm   = false;
        }

        if(!getRePassword().equals(getPassword())) {
            etRePassword.setError(getResources().getString(R.string.label_error_repassword));

            checkForm   = false;
        }

        if (checkForm == true) {
            register();
        }
    }

    public String[] getListSpinner(List<CitiesModel> mList) {
        String[] list;

        if(mList != null && mList.size() > 0) {
            list  = new String[mList.size()];

            for (int i=0; i<mList.size(); i++) {
                list[i] = mList.get(i).name;
            }
        } else {
            list = new String[]{"null"};
        }

        return list;
    }

    private String getUsername() {
        return etUsername.getText().toString();
    }

    private String getEmail() {
        return etEmail.getText().toString();
    }

    private String getPassword() {
        return etPassword.getText().toString();
    }

    private String getRePassword() {
        return etRePassword.getText().toString();
    }

    private String getAlamat() {
        return etAlamat.getText().toString();
    }

    private String getTelp() {
        return etTelp.getText().toString();
    }

    private String getCity() {
        String city = "";

        if(listCities != null && listCities.size() > 0)
            city    = listCities.get(spCity.getSelectedItemPosition()).id + "";

        return city;
    }

    private String getDayOfBirth() {
        return etDayOfBirth.getText().toString();
    }

    @Override
    public void LoginConnection(int status, String message) {
        dismissProgressDialog();

        if(status == 1) {
            ShowToast("Akun Telah Dibuat, Silahkan Login");

            Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            showDialogMessage("Gagal", message);
        }

    }

    @Override
    public void EventsConnection(int status, String message, ArrayList<CitiesModel> mCities) {
        if (status == 1) {
            if (mCities != null && mCities.size() > 0) {
                int category = mCities.get(0).category;

                if (category == 0) {
                    listProvince = mCities;
                    setSpinnerAdapter(spProvince, getListSpinner(mCities));

                } else {
                    listCities = mCities;
                    setSpinnerAdapter(spCity, getListSpinner(mCities));
                }
            }
        }
    }
}

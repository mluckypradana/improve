package com.improve.app.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.util.Attributes;
import com.improve.app.R;
import com.improve.app.adapter.CartAdapter;
import com.improve.app.adapter.CartSwipeAdapter;
import com.improve.app.http.CartConnection;
import com.improve.app.model.CartModel;
import com.improve.app.utils.Util;
import com.improve.app.widget.RotateLoading;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;


/**
 * Created by   yudi rahmat
 * Email        yudirahmat7@gmail.com
 */
public class CartListFragment extends BaseFragment implements CartSwipeAdapter.CartListener, CartConnection.CartConnectionListener {
    private CartSwipeAdapter mAdapter;
    private CartConnection mConnection;

    private SweetAlertDialog progressDialog;
    private RotateLoading mRotateLoading;
    private TextView mEventStatus;
    private ListView mListView;
    private Button btnCheckout;

    private boolean mIsLoading  = false;

    private Activity context;

    private ArrayList<CartModel> mCartList;

    private int cancelPosition = 0;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        context     = activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View viewroot = inflater.inflate(R.layout.fragment_cartlist, container, false);
        return viewroot;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mListView               = (ListView) getView().findViewById(R.id.list);
        mEventStatus            = (TextView) getView().findViewById(R.id.event_status);
        btnCheckout             = (Button) getView().findViewById(R.id.btn_checkout);
        mRotateLoading          = (RotateLoading) getView().findViewById(R.id.rotateloading);

        mConnection             = new CartConnection(this);
        progressDialog          = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);

        getAllSeminarList();
    }

    private void getAllSeminarList() {
        try {
            showProgressDialog();

            mConnection.cart(getUserId());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ((SwipeLayout) (mListView.getChildAt(position - mListView.getFirstVisiblePosition()))).open(true);
            }
        });

        btnCheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmCheckout();
            }
        });
    }

    private void showProgressBar() {
        mIsLoading  = true;

        progressDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.orange));
        progressDialog.setTitleText("Please Wait ...");
        progressDialog.show();
    }

    private void dismissProgressBar() {
        mIsLoading  = false;

        progressDialog.dismiss();
    }

    private void showProgressDialog() {
        mIsLoading  = true;

        mRotateLoading.start();

        mListView.setVisibility(View.GONE);
        btnCheckout.setVisibility(View.GONE);
        mRotateLoading.setVisibility(View.VISIBLE);
    }

    private void dismissProgressDialog() {
        mIsLoading  = false;

        mRotateLoading.stop();

        mListView.setVisibility(View.VISIBLE);
        mRotateLoading.setVisibility(View.GONE);
    }

    private void setDataAdapter() {
        mAdapter    = new CartSwipeAdapter(this, mCartList);

        mAdapter.setMode(Attributes.Mode.Single);
        mListView.setAdapter(mAdapter);
    }

    private void setAllData(int status) {
        if(status == 1 && mCartList != null && mCartList.size() > 0) {
            mEventStatus.setVisibility(View.GONE);
            mListView.setVisibility(View.VISIBLE);
            btnCheckout.setVisibility(View.VISIBLE);

            setDataAdapter();

        } else if(status == 0) {
            mEventStatus.setVisibility(View.VISIBLE);
            mListView.setVisibility(View.GONE);
            //showDialogMessage("Error", message);
            btnCheckout.setVisibility(View.GONE);
        } else {
            mEventStatus.setVisibility(View.VISIBLE);
            mListView.setVisibility(View.GONE);
            btnCheckout.setVisibility(View.GONE);
            //showDialogMessage("Error", message);
        }
    }

    private void confirmCheckout() {
        new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                .setTitleText(context.getResources().getString(R.string.label_dialog_title_checkout))
                .setContentText(context.getResources().getString(R.string.label_dialog_checkout_message))
                .setCancelText(context.getResources().getString(R.string.label_dialog_cancel))
                .setConfirmText(context.getResources().getString(R.string.label_dialog_checkout))
                .showCancelButton(true)
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        // reuse previous dialog instance, keep widget user state, reset them if you need
                        sDialog.setTitleText(context.getResources().getString(R.string.label_dialog_canceled))
                                .setContentText(context.getResources().getString(R.string.label_dialog_checkout_failed))
                                .setConfirmText(context.getResources().getString(R.string.label_dialog_ok))
                                .showCancelButton(false)
                                .setCancelClickListener(null)
                                .setConfirmClickListener(null)
                                .changeAlertType(SweetAlertDialog.ERROR_TYPE);

                    }
                })
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismiss();
                        showProgressBar();

                        mConnection.checkoutCart(getUserId());
                    }
                })
                .show();
    }

    @Override
    public void CartConnection(int status, String message, ArrayList<CartModel> mEvents) {
        dismissProgressBar();
        dismissProgressDialog();

        this.mCartList   = mEvents;

        setAllData(status);

    }

    @Override
    public void CancelCartConnection(int status, String message) {
        dismissProgressBar();
        dismissProgressDialog();

        if(status == 1) {
            mCartList.remove(cancelPosition);
            mAdapter.notifyDataSetChanged();

            setAllData(status);

            showDialogSuccess(context.getResources().getString(R.string.label_dialog_deleted), context.getResources().getString(R.string.label_dialog_data_deleted));
        }

    }

    @Override
    public void CheckoutCartConnection(int status, String messaage) {
        dismissProgressBar();
        dismissProgressDialog();

        if(status == 1) {
            showProgressDialog();

            mConnection.cart(getUserId());

            showDialogSuccess(context.getResources().getString(R.string.label_dialog_success), context.getResources().getString(R.string.label_dialog_checkout_success));
        }

    }

    @Override
    public void DeleteCartListener(int id, String programName, int position) {
        dismissProgressBar();
        dismissProgressDialog();

        showProgressBar();

        mConnection.cancelCart(getUserId(), programName, id);

        cancelPosition = position;
    }
}

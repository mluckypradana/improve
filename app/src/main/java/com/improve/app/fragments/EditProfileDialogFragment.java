package com.improve.app.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.improve.app.R;
import com.improve.app.http.CityConnection;
import com.improve.app.http.RegisterConnection;
import com.improve.app.model.CitiesModel;
import com.improve.app.model.User;
import com.improve.app.service.response.CitiesResponse;
import com.improve.app.service.response.UserDetailResponse;
import com.improve.app.utils.Cons;
import com.improve.app.utils.FileUtils;
import com.improve.app.utils.Util;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.RequestBody;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;


/**
 * Created by   MuhammadLucky
 */
@SuppressLint("ValidFragment")
public class EditProfileDialogFragment extends BaseDialogFragment implements RegisterConnection.RegisterConnectionListener, CityConnection.CitiesConnectionListener {

    private final ProfileFragment profileFragment;
    @Bind(R.id.et_username)
    EditText etUsername;
    @Bind(R.id.et_email)
    EditText etEmail;
    @Bind(R.id.et_password)
    EditText etPassword;
    @Bind(R.id.et_comfirm_password)
    EditText etRePassword;
    @Bind(R.id.et_city)
    EditText etCity;
    @Bind(R.id.et_alamat)
    EditText etAddress;
    @Bind(R.id.et_telp)
    EditText etPhone;
    @Bind(R.id.et_tanggal_lahir)
    EditText etDayOfBirth;
    @Bind(R.id.img_add_audit)
    ImageView ivAddFoto;
    @Bind(R.id.b_update)
    Button bUpdate;
    @Bind(R.id.sp_province)
    Spinner spProvince;
    @Bind(R.id.sp_city)
    Spinner spCity;
    @Bind(R.id.ll_place)
    LinearLayout llPlace;

    private RegisterConnection mConnection;
    private SharedPreferences mSharedPreferences;
    private CityConnection mCityConnection;

    private SweetAlertDialog progressDialog;
    private ProgressDialog mProgressDialog;

    private List<CitiesModel> listProvince;
    private List<CitiesModel> listCities;

    private String mBirthday;
    private String imagePath;
    private String province;
    private String city;
    private Uri imgUri;

    private int INTENT_CAMERA = 101;
    private int INTENT_GALLERY = 102;

    private boolean setCity = false;

    /* Calendar get time */
    Calendar myCal = Calendar.getInstance();
    DatePickerDialog.OnDateSetListener dpl = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            myCal.set(Calendar.YEAR, year);
            myCal.set(Calendar.MONTH, monthOfYear);
            myCal.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            mBirthday = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
            String birthdayText = dayOfMonth + " " + Util.month[monthOfYear] + " " + year;
            etDayOfBirth.setText(birthdayText);
        }
    };
    private int id;
    private List<CitiesModel> provinces = new ArrayList<>();
    private List<CitiesModel> cities = new ArrayList<>();

    @SuppressLint("ValidFragment")
    public EditProfileDialogFragment(ProfileFragment profileFragment) {
        this.profileFragment = profileFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        View view = inflater.inflate(R.layout.activity_update_profile, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
            dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mSharedPreferences      = getActivity().getSharedPreferences(Cons.PRIVATE_PREF, Context.MODE_PRIVATE);

        mConnection             = new RegisterConnection(this);
        mCityConnection         = new CityConnection(this);

        //Hide province and city form since there's no parameter in update user API
        llPlace.setVisibility(View.VISIBLE);

        mProgressDialog = new ProgressDialog(getActivity());
        progressDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);

        onClickData();
        disableSoftInputFromAppearing(etDayOfBirth);

        setProfileData();

        //getProvinces();
        showAllList();
        Util.createAppDir("Data/Profil/Photo");
    }

    private void showChoosePhoto() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final CharSequence[] array  = {"Take a Photo", "Choose from Gallery"};

        builder
                .setSingleChoiceItems(array, -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(which == 0) {
                            intentToCamera();
                        } else if(which == 1) {
                            intentToGallery();
                        }

                        dialog.dismiss();
                    }
                });

        builder.create();
        builder.show();
    }

    private void intentToGallery() {
        Intent tempIntent = new Intent(Intent.ACTION_GET_CONTENT);
        tempIntent.setType("image/*");
        PackageManager pm = getActivity().getPackageManager();
        Intent openInChooser = Intent.createChooser(tempIntent, getResources().getString(R.string.select_picture));
        startActivityForResult(openInChooser, INTENT_GALLERY);
    }

    private void intentToCamera() {
        try {
            /*Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, INTENT_CAMERA);*/

            String fileName = System.currentTimeMillis() + ".jpg";
            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.TITLE, fileName);
            values.put(MediaStore.Images.Media.DESCRIPTION,"Image capture by camera");
            imgUri = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

            Intent intent = new Intent( MediaStore.ACTION_IMAGE_CAPTURE );
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imgUri);
            startActivityForResult(intent, INTENT_CAMERA);
        } catch (Exception e) {
            e.printStackTrace();

            ShowToast(getActivity().getResources().getString(R.string.label_toast_check_permission_camera));
        }
    }

    private void showAllList() {
        setSpinnerAdapter(spProvince, new String[]{"Load Data ..."});
        setSpinnerAdapter(spCity, new String[]{"Load Data ..."});

        mCityConnection.getCities(0);

        spProvince.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //connectionStatus    = mConnection.isConnectingToInternet();

                setSpinnerAdapter(spCity,  new String[] {"Load Data ..."});

                if(listProvince != null && listProvince.size() > 0)
                    mCityConnection.getCities(listProvince.get(position).id);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) { }
        });
    }

    public String[] getListSpinner(List<CitiesModel> mList) {
        String[] list;

        if (mList != null && mList.size() > 0) {
            list = new String[mList.size()];

            for (int i = 0; i < mList.size(); i++) {
                list[i] = mList.get(i).name;
            }
        } else {
            list = new String[]{"null"};
        }

        return list;
    }

    public void setSpinnerAdapter(Spinner spinners, String[] list) {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), R.layout.item_list_spinner, list);
        adapter.setDropDownViewResource(R.layout.item_list_spinner_dropdown);
        spinners.setAdapter(adapter);
    }

    private void setProfileData() {
        id                  = mSharedPreferences.getInt(Cons.PACKAGE_PREFIX + "user_id", 0);
        String email        = mSharedPreferences.getString(Cons.PACKAGE_PREFIX + "user_email", "");
        String username     = mSharedPreferences.getString(Cons.PACKAGE_PREFIX + "user_username", "");
        String birthday     = mSharedPreferences.getString(Cons.PACKAGE_PREFIX + "user_birthday", "");
        String phone        = mSharedPreferences.getString(Cons.PACKAGE_PREFIX + "user_phone", "");
        String address      = mSharedPreferences.getString(Cons.PACKAGE_PREFIX + "user_address", "");
        province            = mSharedPreferences.getString(Cons.PACKAGE_PREFIX + "user_province", "");
        city                = mSharedPreferences.getString(Cons.PACKAGE_PREFIX + "user_city", "");
        String photo        = mSharedPreferences.getString(Cons.PACKAGE_PREFIX + "user_photo", "");

        String birthdayText = "";
        if (birthday != null && !birthday.equals("")) {
            String[] birthdayTexts = birthday.split("-");
            String day = birthdayTexts[0];
            String month = birthdayTexts[1];
            String year = birthdayTexts[2];
            birthdayText = day + " " + Util.month[Integer.valueOf(month)] + " " + year;
        }

        etDayOfBirth.setText(birthdayText);

        etUsername.setText(username);
        etEmail.setText(email);
        etCity.setText(city);
        etAddress.setText(address);
        etPhone.setText(phone);
        etDayOfBirth.setText(birthday);
        Picasso.with(getActivity()).load(Cons.API_URL + photo).into(ivAddFoto);

        bUpdate.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.b_update)
    void updateProfile() {
        checkValidationForm();
    }

    private void onClickData() {
        ivAddFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    showChoosePhoto();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        etDayOfBirth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog dpd = new DatePickerDialog(getActivity(), dpl,
                        myCal.get(Calendar.YEAR),
                        myCal.get(Calendar.MONTH),
                        myCal.get(Calendar.DAY_OF_MONTH));
                dpd.show();
            }
        });
    }

    @SuppressLint("NewApi")
    private void disableSoftInputFromAppearing(final EditText editText) {
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

        if (Build.VERSION.SDK_INT >= 11) {
            editText.setRawInputType(InputType.TYPE_CLASS_TEXT);
            editText.setTextIsSelectable(true);
        } else {
            editText.setRawInputType(InputType.TYPE_NULL);
            editText.setFocusable(true);
        }

        etDayOfBirth.setInputType(0);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        /*if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 1) {
                Bundle extras = data.getExtras();
                Bitmap bitMap = (Bitmap) extras.get("data");
                String[] projection = {MediaStore.Images.Media.DATA};
                Cursor cursor = getActivity().managedQuery(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection, null, null, null);
                int column_index_data = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToLast();
                imagePath = cursor.getString(column_index_data);
//                Bitmap bitmap = Util.decodeSampledBitmapFromUri(imagePath, 480, 800);

                //Util.saveBitmapToFile(imagePath, bitmap);
                ivAddFoto.setImageBitmap(bitMap);
            }
        }*/

        if (resultCode == Activity.RESULT_OK) {
            if(requestCode == INTENT_CAMERA) {
                imagePath = getImagePath(imgUri);

                String fotoPath         = Util.getDir("Data/Profil/Photo/") + Util.getPhotoName(imagePath);

                Bitmap bitmap           = Util.decodeSampledBitmapFromUri(fotoPath, 800, 1280);
                Util.saveBitmapToFile(fotoPath, bitmap);

                imagePath = fotoPath;

                File imgFile = new File(imagePath);
                if(imgFile.exists()){
                    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    ivAddFoto.setImageBitmap(myBitmap);
                }


            } else if(requestCode == INTENT_GALLERY) {
                String path     = FileUtils.getPath(getActivity(), data.getData());

                Bitmap bitmap   = Util.decodeSampledBitmapFromUri(path, 800, 1280);

                String fotoPath = Util.getDir("Data/Profil/Photo/") + Util.getPhotoName(path);

                imagePath       = fotoPath;

                Util.saveBitmapToFile(fotoPath, bitmap);

                File imgFile = new File(fotoPath);
                if(imgFile.exists()){
                    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    ivAddFoto.setImageBitmap(myBitmap);
                }

            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.verify:
                checkValidationForm();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private String getCity() {
        String city = "";

        if(listCities != null && listCities.size() > 0)
            city    = listCities.get(spCity.getSelectedItemPosition()).id + "";

        return city;
    }

    private String getPasswords() {
        String password = etPassword.getText().toString();

        if(password.isEmpty()) {
            password    = mSharedPreferences.getString(Cons.PACKAGE_PREFIX + "user_password", null);
        }

        return password;
    }

    private void updateUser() {
        showProgressDialog();
        try {
            if (imagePath == null)
                imagePath = "";
            mConnection.regisetrOrUpdate(
                    true,
                    id,
                    imagePath,
                    etEmail.getText().toString(),
                    getCity(),
                    etAddress.getText().toString(),
                    etPhone.getText().toString(),
                    mBirthday,
                    etUsername.getText().toString(),
                    getPasswords());
        } catch (Exception e) {
            dismissProgressDialog();
            e.printStackTrace();
        }
    }

    @Deprecated
    private void oldUpdateUser() {
        showProgressDialog();

        RequestBody photoBody = null;
        if (imagePath != null && !imagePath.equals(""))
            photoBody = RequestBody.create(MediaType.parse("image/*"), new File(imagePath));

        Map<String, RequestBody> map = new HashMap<>();
        map.put("user[email]", toRequestBody(etEmail.getText().toString()));
        map.put("user[password]", toRequestBody(etPassword.getText().toString()));
        map.put("user[username]", toRequestBody(etUsername.getText().toString()));
        map.put("user[city]", toRequestBody(etEmail.getText().toString()));
        map.put("user[address]", toRequestBody(etEmail.getText().toString()));
        map.put("user[phone]", toRequestBody(etEmail.getText().toString()));
        map.put("user[birthday]", toRequestBody(etEmail.getText().toString()));
        map.put("user[photo]", toRequestBody(etEmail.getText().toString()));

        if (imagePath != null) {
            File file = new File(imagePath);
            RequestBody fileBody = RequestBody.create(MediaType.parse("image/png"), file);
            map.put("user[photo]\"; filename=\"pp.png\"", fileBody);
        }

        Call<UserDetailResponse> call = getService().updateUser(id, map);
        call.enqueue(new Callback<UserDetailResponse>() {
            @Override
            public void onResponse(Response<UserDetailResponse> response, Retrofit retrofit) {
                UserDetailResponse userResponse = response.body();
                saveProfileToPreference(userResponse);
                dismissProgressDialog();
                dismiss();
            }

            @Override
            public void onFailure(Throwable t) {
                showDialogMessage(getResources().getString(R.string.label_dialog_failed_regist), getResources().getString(R.string.failed_server_failure));
                dismissProgressDialog();
            }
        });
    }


    public static RequestBody toRequestBody(String value) {
        RequestBody body = RequestBody.create(MediaType.parse("text/plain"), value);
        return body;
    }

    private void saveProfileToPreference(UserDetailResponse response) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        if (response == null)
            return;
        User user = response.getData();
        editor.putInt(Cons.PACKAGE_PREFIX + "user_id",              user.id);
        editor.putString(Cons.PACKAGE_PREFIX + "user_email",        user.email);
        editor.putString(Cons.PACKAGE_PREFIX + "user_password",     getPasswords());
        editor.putString(Cons.PACKAGE_PREFIX + "user_created_at",   user.created_at);
        editor.putString(Cons.PACKAGE_PREFIX + "user_updated_at",   user.updated_at);
        editor.putString(Cons.PACKAGE_PREFIX + "user_role_id",      user.role_id);
        editor.putString(Cons.PACKAGE_PREFIX + "user_role_name",    user.role_name);
        editor.putString(Cons.PACKAGE_PREFIX + "user_username",     user.username);
        editor.putString(Cons.PACKAGE_PREFIX + "user_birthday",     user.birthday);
        editor.putString(Cons.PACKAGE_PREFIX + "user_phone",        user.phone);
        editor.putString(Cons.PACKAGE_PREFIX + "user_address",      user.address);
        editor.putString(Cons.PACKAGE_PREFIX + "user_province",     user.province);
        editor.putString(Cons.PACKAGE_PREFIX + "user_city",         user.city);
        editor.putString(Cons.PACKAGE_PREFIX + "user_photo",        user.photo);
        editor.apply();
    }

    private void showProgressDialog() {

        progressDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.orange));
        progressDialog.setTitleText("Please Wait ...");
        progressDialog.show();

        /*mProgressDialog.setMessage("Please Wait...");
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();*/
    }

    private void dismissProgressDialog() {

        mProgressDialog.dismiss();
        progressDialog.dismiss();
    }

    private void checkValidationForm() {
        boolean checkForm = true;

        if (getDayOfBirth().isEmpty() || getDayOfBirth().equals(getResources().getString(R.string.label_error_hint))) {
            etDayOfBirth.setError(getResources().getString(R.string.label_error_hint));

            checkForm = false;
        }

        if (getTelp().isEmpty()) {
            etPhone.setError(getResources().getString(R.string.label_error_hint));

            checkForm = false;
        }

        if (getAlamat().isEmpty()) {
            etAddress.setError(getResources().getString(R.string.label_error_hint));

            checkForm = false;
        }

        if (getEmail().isEmpty()) {
            etEmail.setError(getResources().getString(R.string.label_error_hint));

            checkForm = false;
        }

        if (getUsername().isEmpty()) {
            etUsername.setError(getResources().getString(R.string.label_error_hint));

            checkForm = false;
        }

        if(!getPassword().isEmpty()) {
            if (getPassword().length() < 8) {
                etPassword.setError(getResources().getString(R.string.label_error_password));

                checkForm = false;
            }

            if (!getRePassword().equals(getPassword())) {
                etRePassword.setError(getResources().getString(R.string.label_error_repassword));

                checkForm = false;
            }
        }

        if (checkForm)
            updateUser();
    }

    private String getUsername() {
        return etUsername.getText().toString();
    }

    private String getEmail() {
        return etEmail.getText().toString();
    }

    private String getPassword() {
        return etPassword.getText().toString();
    }

    private String getRePassword() {
        return etRePassword.getText().toString();
    }

    private String getAlamat() {
        return etAddress.getText().toString();
    }

    private String getTelp() {
        return etPhone.getText().toString();
    }

    private String getDayOfBirth() {
        return etDayOfBirth.getText().toString();
    }

    @Override
    public void LoginConnection(int status, String message) {
        dismissProgressDialog();

        if (status == 1) {
            dismiss();
            if (profileFragment != null)
                profileFragment.updateData();
        } else
            showDialogMessage("Gagal", message);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void EventsConnection(int status, String message, ArrayList<CitiesModel> mCities) {
        if (status == 1) {
            if (mCities != null && mCities.size() > 0) {
                int category = mCities.get(0).category;

                if (category == 0) {
                    listProvince = mCities;
                    setSpinnerAdapter(spProvince, getListSpinner(mCities));

                } else {
                    listCities = mCities;
                    setSpinnerAdapter(spCity, getListSpinner(mCities));
                }
            }

            setProvinceAndCity();
        }
    }

    private void setProvinceAndCity() {
        if(setCity == false) {
            if(listProvince != null && listProvince.size() > 0) {
                for(int i=0; i<listProvince.size(); i++) {
                    if(province.toLowerCase().equals(listProvince.get(i).name.toLowerCase())) {
                        spProvince.setSelection(i);

                        break;
                    }
                }
            }

            if(listCities != null && listCities.size() > 0) {
                for(int i=0; i<listCities.size(); i++) {
                    if(city.toLowerCase().equals(listCities.get(i).name.toLowerCase())) {
                        spCity.setSelection(i);
                        setCity = false;

                        break;
                    }
                }
            }
        }
    }
}

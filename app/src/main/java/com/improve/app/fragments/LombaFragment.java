package com.improve.app.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.improve.app.R;
import com.improve.app.activities.DetailLombaActivity;
import com.improve.app.adapter.LombaAdapter;
import com.improve.app.http.LombaConnection;
import com.improve.app.interfaceview.IFilterView;
import com.improve.app.model.EventLomba;
import com.improve.app.utils.Cons;
import com.improve.app.utils.Util;
import com.improve.app.widget.RotateLoading;

import java.util.ArrayList;


/**
 * Created by   yudi rahmat
 * Email        yudirahmat7@gmail.com
 */
public class LombaFragment extends BaseFragment implements LombaConnection.EventsConnectionListener, IFilterView {
    private LombaAdapter mAdapter;
    private LombaConnection mConnection;

    private RotateLoading mRotateLoading;
    private TextView mEventStatus;
    ListView mListView;

    //private ProgressDialog mProgressDialog;
    private boolean mIsLoading  = false;

    private Activity context;

    private ArrayList<EventLomba> mEvents;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        context     = activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View viewroot = inflater.inflate(R.layout.fragment_event, container, false);
        mListView               = (ListView) viewroot.findViewById(R.id.list);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent   = new Intent(getActivity(), DetailLombaActivity.class);
                Log.d("WOY", "masuk");
                intent.putExtra(Cons.PACKAGE_PREFIX + "lombaId", mEvents.get(position).id);

                getActivity().startActivity(intent);
            }
        });
        return viewroot;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mEventStatus            = (TextView) getView().findViewById(R.id.event_status);
        mRotateLoading          = (RotateLoading) getView().findViewById(R.id.rotateloading);

        mAdapter                = new LombaAdapter(context);
        //mProgressDialog         = new ProgressDialog(context);
        mConnection             = new LombaConnection(this);

        getAllSeminarList(false, null, 0, null);
    }

    private void getAllSeminarList(boolean filter, String title, int cityId, String category2) {
        try {
            showProgressDialog();

            mConnection.Events("lomba", filter, title, cityId, category2);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void showProgressDialog() {
        mIsLoading  = true;

        mRotateLoading.start();

        mListView.setVisibility(View.GONE);
        mRotateLoading.setVisibility(View.VISIBLE);

        /*mProgressDialog.setMessage("Please Wait...");
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();*/
    }

    private void dismissProgressDialog() {
        mIsLoading  = false;

        mRotateLoading.stop();

        mListView.setVisibility(View.VISIBLE);
        mRotateLoading.setVisibility(View.GONE);

        //mProgressDialog.dismiss();
    }

    @Override
    public void EventsConnection(int status, String message, ArrayList<EventLomba> mEvents) {
        dismissProgressDialog();

        if(status == 1 && mEvents != null && mEvents.size() > 0) {
            this.mEvents   = mEvents;

            mAdapter.setArray(mEvents);
            mListView.setAdapter(mAdapter);
            mEventStatus.setVisibility(View.GONE);
//
//            mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                @Override
//                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                    //startNewActivity(DetailEventActivity.class);
//                }
//            });
        } else if(status == 0) {
            mEventStatus.setVisibility(View.VISIBLE);
            mListView.setVisibility(View.GONE);
            //showDialogMessage("Error", message);
        } else {
            mEventStatus.setVisibility(View.VISIBLE);
            mListView.setVisibility(View.GONE);
            //showDialogMessage("Error", message);
        }
    }

    @Override
    public void filterResponse(String type, String searchTitle, int searchCityId, String searchCategory) {
        if(type.equals(Util.categoryFilter[1])) {
            getAllSeminarList(true, searchTitle, searchCityId, searchCategory);
        }

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (!isVisibleToUser) {

        } else {
            setListenerFilter(this);
        }
    }
}

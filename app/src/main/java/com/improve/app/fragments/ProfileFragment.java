package com.improve.app.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewAnimator;

import com.improve.app.R;
import com.improve.app.http.DetectConnection;
import com.improve.app.model.User;
import com.improve.app.service.response.UserDetailResponse;
import com.improve.app.utils.Cons;
import com.improve.app.utils.Debug;
import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;

import java.util.Calendar;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class ProfileFragment extends BaseFragment {
    @Bind(R.id.va_main)
    ViewAnimator vaMain;
    @Bind(R.id.iv_photo)
    RoundedImageView ivPhoto;
    @Bind(R.id.tv_name)
    TextView tvName;
    @Bind(R.id.tv_age)
    TextView tvAge;
    @Bind(R.id.tv_about)
    TextView tvAbout;
    @Bind(R.id.ll_about)
    LinearLayout llAbout;
    @Bind(R.id.tv_profile_title)
    TextView tvProfileTitle;
    @Bind(R.id.tv_email)
    TextView tvEmail;
    @Bind(R.id.tv_full_name)
    TextView tvFullName;
    @Bind(R.id.tv_phone)
    TextView tvPhone;
    @Bind(R.id.ll_profile)
    LinearLayout llProfile;
    @Bind(R.id.tv_address)
    TextView tvAddress;

    private SharedPreferences preferences;
    private int id;
    private String email, createdAt, updatedAt, roleId, roleName, username, birthday, phone, address, province, city, photo;
    private SharedPreferences.Editor editor;
    private EditProfileDialogFragment editProfiledialogFragment;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View viewroot = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, viewroot);
        return viewroot;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        preferences = getActivity().getSharedPreferences(Cons.PRIVATE_PREF, Context.MODE_PRIVATE);

        getProfile();

        Debug.i(Cons.TAG, "cek123 " + preferences.getString(Cons.PACKAGE_PREFIX + "user_birthday", ""));
    }

    /**
     * Show edit name dialog
     */
    @OnClick(R.id.tv_profile_title)
    void showEditForm() {
        editProfiledialogFragment = new EditProfileDialogFragment(this);
        editProfiledialogFragment.show(getFragmentManager(), "dialog");
    }

    /**
     * Get profile from preferences
     */

    private void getProfile() {
        if (new DetectConnection(getActivity()).isConnectingToInternet())
            getOnlineProfile();
        else
            getOfflineProfile();
    }

    private void getOnlineProfile() {
        id = preferences.getInt(Cons.PACKAGE_PREFIX + "user_id", 0);
        Call<UserDetailResponse> call = getService().getUserDetail(id);
        vaMain.setDisplayedChild(LAYOUT_LOADING);
        call.enqueue(new Callback<UserDetailResponse>() {
            @Override
            public void onResponse(Response<UserDetailResponse> response, Retrofit retrofit) {
                UserDetailResponse userResponse = response.body();
                saveProfileToPreference(userResponse);
                showProfile(userResponse.getData());

                Debug.i(Cons.TAG, "response " + response.toString());

                vaMain.setDisplayedChild(LAYOUT_MAIN);
            }

            @Override
            public void onFailure(Throwable t) {
                getOfflineProfile();
                vaMain.setDisplayedChild(LAYOUT_MAIN);
            }
        });
    }

    private void saveProfileToPreference(UserDetailResponse response) {
        editor = preferences.edit();
        User user = response.getData();
        editor.putInt(Cons.PACKAGE_PREFIX + "user_id", user.id);
        editor.putString(Cons.PACKAGE_PREFIX + "user_email", user.email);
        editor.putString(Cons.PACKAGE_PREFIX + "user_created_at", user.created_at);
        editor.putString(Cons.PACKAGE_PREFIX + "user_updated_at", user.updated_at);
        editor.putString(Cons.PACKAGE_PREFIX + "user_role_id", user.role_id);
        editor.putString(Cons.PACKAGE_PREFIX + "user_role_name", user.role_name);
        editor.putString(Cons.PACKAGE_PREFIX + "user_username", user.username);
        editor.putString(Cons.PACKAGE_PREFIX + "user_birthday", user.birthday);
        editor.putString(Cons.PACKAGE_PREFIX + "user_phone", user.phone);
        editor.putString(Cons.PACKAGE_PREFIX + "user_address", user.address);
        editor.putString(Cons.PACKAGE_PREFIX + "user_province", user.province);
        editor.putString(Cons.PACKAGE_PREFIX + "user_city", user.city);
        editor.putString(Cons.PACKAGE_PREFIX + "user_photo", user.photo);
        editor.commit();
    }

    /**
     * Get offline profile from database
     */
    private void getOfflineProfile() {
        id = preferences.getInt(Cons.PACKAGE_PREFIX + "user_id", 0);
        email = preferences.getString(Cons.PACKAGE_PREFIX + "user_email", "");
        createdAt = preferences.getString(Cons.PACKAGE_PREFIX + "user_created_at", "");
        updatedAt = preferences.getString(Cons.PACKAGE_PREFIX + "user_updated_at", "");
        roleId = preferences.getString(Cons.PACKAGE_PREFIX + "user_role_id", "");
        roleName = preferences.getString(Cons.PACKAGE_PREFIX + "user_role_name", "");
        username = preferences.getString(Cons.PACKAGE_PREFIX + "user_username", "");
        birthday = preferences.getString(Cons.PACKAGE_PREFIX + "user_birthday", "");
        phone = preferences.getString(Cons.PACKAGE_PREFIX + "user_phone", "");
        address = preferences.getString(Cons.PACKAGE_PREFIX + "user_address", "");
        province = preferences.getString(Cons.PACKAGE_PREFIX + "user_province", "");
        city = preferences.getString(Cons.PACKAGE_PREFIX + "user_city", "");
        photo = preferences.getString(Cons.PACKAGE_PREFIX + "user_photo", "");

        //Create user object to show profile
        User user = new User();
        user.id = id;
        user.email = email;
        user.created_at = createdAt;
        user.updated_at = updatedAt;
        user.role_id = roleId;
        user.role_name = roleName;
        user.username = username;
        user.birthday = birthday;
        user.phone = phone;
        user.address = address;
        user.province = province;
        user.city = city;
        user.photo = photo;

        showProfile(user);
    }

    /**
     * Show user object to layout
     *
     * @param user
     */
    public void showProfile(User user) {
        Picasso.with(getActivity()).load(Cons.API_URL + user.photo).placeholder(R.drawable.icon_nav_profil)
                .error(R.drawable.icon_eo_sample).into(ivPhoto);
        tvName.setText(user.username);

        try {
            String[] birthdayTexts = user.birthday.split("-");
            int age = getAge(Integer.parseInt(birthdayTexts[0]), Integer.parseInt(birthdayTexts[1]), Integer.parseInt(birthdayTexts[2]));
            if (age > 0)
                tvAge.setText(getResources().getString(R.string.label_age) + " : " + age);
            else
                tvAge.setVisibility(View.GONE);
        } catch (Exception e) {
            tvAge.setVisibility(View.GONE);
            e.printStackTrace();
            Debug.i(Cons.TAG, "response " + user.birthday);
        }

        //TODO Show full name
        tvFullName.setVisibility(View.GONE);
        tvEmail.setText(getResources().getString(R.string.label_email) + " : " + user.email);
        tvPhone.setText(getResources().getString(R.string.label_phone) + " : " + user.phone);
        tvAddress.setText(getResources().getString(R.string.label_address) + " : " + user.address + ", " + user.city);
    }

    public static int getAge(int year, int month, int day) {
        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();
        dob.set(year, month, day);
        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR))
            age--;
        return age;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    public void updateData() {
        getProfile();
    }
}
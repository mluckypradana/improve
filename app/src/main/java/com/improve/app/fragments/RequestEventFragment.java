package com.improve.app.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.improve.app.R;
import com.improve.app.activities.LoginActivity;
import com.improve.app.http.CityConnection;
import com.improve.app.http.DetectConnection;
import com.improve.app.http.RequestEventConnection;
import com.improve.app.model.CitiesModel;
import com.improve.app.utils.Cons;
import com.improve.app.utils.Debug;
import com.improve.app.utils.GPSTracker;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by Yudi on 2/2/2016.
 */
public class RequestEventFragment extends BaseFragment implements CityConnection.CitiesConnectionListener, RequestEventConnection.RequestConnectionListener {
    private DetectConnection mConnection;
    private RequestEventConnection mRequestConnection;
    private CityConnection mCityConnection;

    private SharedPreferences sharedPreference;
    private SharedPreferences.Editor editor;

    private SweetAlertDialog progressDialog;
    private ProgressDialog mProgressDialog;
    private boolean mIsLoading  = false;

    private EditText etTitle, etDescription, etOtherCategory;
    private Spinner spCategory, spProvince, spCity;

    private List<CitiesModel> listcCategory;
    private List<CitiesModel> listProvince;
    private List<CitiesModel> listCities;

    private boolean connectionStatus = false;
    private boolean GpsStatus = false;
    
    private boolean mCheckRequest = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_request_event, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        sharedPreference	= getActivity().getSharedPreferences(Cons.PRIVATE_PREF, Context.MODE_PRIVATE);
        editor				= sharedPreference.edit();

        etTitle             = (EditText) view.findViewById(R.id.et_request_event_title);
        etDescription       = (EditText) view.findViewById(R.id.et_request_event_description);
        etOtherCategory     = (EditText) view.findViewById(R.id.et_request_event_other_category);
        spCity              = (Spinner) view.findViewById(R.id.sp_city);
        spProvince          = (Spinner) view.findViewById(R.id.sp_province);
        spCategory          = (Spinner) view.findViewById(R.id.sp_category);

        mConnection         = new DetectConnection(getActivity());
        mCityConnection     = new CityConnection(this);
        mRequestConnection  = new RequestEventConnection(this);
        mProgressDialog     = new ProgressDialog(getActivity());
        progressDialog      = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);

        connectionStatus    = mConnection.isConnectingToInternet();

        setSpinnerAdapter(spCategory,  new String[] {"Load Data ..."});
        setSpinnerAdapter(spProvince,  new String[] {"Load Data ..."});
        setSpinnerAdapter(spCity,  new String[] {"Load Data ..."});

        showAllList();
        checkLoginStatus();
    }

    private void checkLoginStatus() {
        if(getUserId() == 0) {
            showDialogMessage("Pemberitahuan", "Anda Perlu Login Dahulu Untuk Request Event");
        }
    }

    private void sendRequestEvent() {
        EnableGps();
        getLatLng();

        if(GpsStatus == true) {
            if(getCity().isEmpty() || getCity() == null || getTitle().isEmpty() || getTitle() == null) {
                showDialogMessage("Pemberitahuan", "Semua Data Harus Diisi");
            } else {
                mRequestConnection.requestEvent(getTitle(), getDescription(), getAddress(), getUserId() + "", getLatitude(), getLongitude(), getCategory(), getCity(), getOtherCaategory());
            }
        } else {
            ShowToast("Silahkan Aktifkan GPS Anda");
        }
    }

    private void EnableGps() {
        LocationManager locationManager = (LocationManager) getActivity().getSystemService(getActivity().LOCATION_SERVICE);
        boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        boolean isGPSEnable = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (isNetworkEnabled == true || isGPSEnable == true) {
            GpsStatus = true;
        } else {
            GpsStatus = false;

            Intent callGPSSettingIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(callGPSSettingIntent);
        }
    }

    private void showProgressDialog() {
        mIsLoading  = true;

        progressDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.orange));
        progressDialog.setTitleText("Please Wait ...");
        progressDialog.show();

        /*mProgressDialog.setMessage("Please Wait...");
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();*/
    }

    private void dismissProgressDialog() {
        mIsLoading  = false;

        mProgressDialog.dismiss();
        progressDialog.dismiss();
    }


    private void showAllList() {
        mCityConnection.getCities(-1);

        spProvince.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                connectionStatus    = mConnection.isConnectingToInternet();

                setSpinnerAdapter(spCity,  new String[] {"Load Data ..."});

                if(listProvince != null && listProvince.size() > 0)
                    mCityConnection.getCities(listProvince.get(position).id);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) { }
        });
    }

    public String[] getListSpinner(List<CitiesModel> mList) {
        String[] list;

        if(mList != null && mList.size() > 0) {
            list  = new String[mList.size()];

            for (int i=0; i<mList.size(); i++) {
                list[i] = mList.get(i).name;
            }
        } else {
            list = new String[]{"null"};
        }

        return list;
    }

    private void getLatLng() {
        GPSTracker mGPSTracker = new GPSTracker(getActivity());
        if (mConnection.isConnectingToInternet() == true) {
            if (mGPSTracker.canGetLocation()) {
                double mlatitude	= mGPSTracker.getLatitude();
                double mlongitude	= mGPSTracker.getLongitude();

                if(mlatitude != 0.0 && mlongitude != 0.0) {
                    editor.putString(Cons.PACKAGE_PREFIX + "latitude", mlatitude+"");
                    editor.putString(Cons.PACKAGE_PREFIX + "longitude", mlongitude+"");
                    editor.commit();
                }

                Debug.i(Cons.TAG, mlatitude + " - " + mlongitude);
            } else {
                Debug.i(Cons.TAG, "on gps failed, please check");

            }
        } else {
            Debug.i(Cons.TAG, "no connection");

            if(mGPSTracker != null)
                mGPSTracker.stopUsingGPS();
        }

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_add, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.verify:
                connectionStatus = mConnection.isConnectingToInternet();
                showProgressDialog();

                if(connectionStatus == true) {
                    if(getUserId() > 0) {
                        sendRequestEvent();
                    } else {
                        ShowToast("Silahkan Login Dahulu");
                        startNewActivity(LoginActivity.class);
                    }
                } else {
                    showDialogMessage("Pemberitahuan", "Silahkan Aktifkan Internet Anda");
                }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void EventsConnection(int status, String message, ArrayList<CitiesModel> mCities) {
        if(status == 1) {
            if(mCities != null && mCities.size() > 0) {
                int category = mCities.get(0).category;

                if(category < 0) {
                    listcCategory    = mCities;
                    setSpinnerAdapter(spCategory, getListSpinner(mCities));

                } else if(category == 0) {
                    listProvince    = mCities;
                    setSpinnerAdapter(spProvince, getListSpinner(mCities));

                } else {
                    listCities      = mCities;
                    setSpinnerAdapter(spCity, getListSpinner(mCities));
                }
            }

        }

        if(mCheckRequest == false) {
            mCityConnection.getCities(0);
            
            mCheckRequest = true;
        }
    }

    private String getTitle() {
        return etTitle.getText().toString();
    }

    private String getDescription() {
        return etDescription.getText().toString();
    }

    private String getAddress() {
        return sharedPreference.getString(Cons.PACKAGE_PREFIX + "user_address", "");
    }

    private String getCity() {
        String city = "";

        if(listCities != null && listCities.size() > 0)
            city    = listCities.get(spCity.getSelectedItemPosition()).id + "";

        return city;
    }

    private String getCategory() {
        String category = "";

        if(listcCategory != null && listcCategory.size() > 0)
            category    = listcCategory.get(spCategory.getSelectedItemPosition()).id + "";

        return category;
    }

    private String getOtherCaategory() {
        return etOtherCategory.getText().toString();
    }

    @Override
    public void ResquestEventResponse(int status, String message) {
        dismissProgressDialog();

        if(status == 1) {
           showDialogMessage("Berhasil", "Request Telah Terkirim");
        } else {
            showDialogMessage("Gagal", "Request Event Gagal");
        }
    }
}

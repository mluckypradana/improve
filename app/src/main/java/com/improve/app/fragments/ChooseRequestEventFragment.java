package com.improve.app.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.improve.app.R;
import com.improve.app.activities.LoginActivity;
import com.improve.app.activities.RequestEventActivity;
import com.improve.app.interfaceview.IFilterView;
import com.improve.app.utils.Util;

/**
 * Created by Yudi on 2/20/2016.
 */
public class ChooseRequestEventFragment extends BaseFragment implements View.OnClickListener, IFilterView {
    private Button btnVoteEvent, btnRequestEvent;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View viewroot = inflater.inflate(R.layout.fragment_choose_request_event, container, false);
        return  viewroot;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btnVoteEvent        = (Button) view.findViewById(R.id.btn_vote_request);
        btnRequestEvent     = (Button) view.findViewById(R.id.btn_request_event);

        btnRequestEvent.setOnClickListener(this);
        btnVoteEvent.setOnClickListener(this);
    }

    private boolean getStatusLogin() {
        boolean status  = false;

        if (Util.getUserId(getActivity()) == null || Util.getUserId(getActivity()).equals("") || getUserId() == 0) {
            ShowToast("Silahkan Login Dahulu");

            status  = false;

            startNewActivity2(LoginActivity.class);

        } else {
            status  = true;
        }

        return status;
    }

    @Override
    public void onClick(View v) {
        int vId = v.getId();

        if(vId == R.id.btn_vote_request) {

        } else if(vId == R.id.btn_request_event) {
            if(getStatusLogin() == true)
                startNewActivity2(RequestEventActivity.class);
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (!isVisibleToUser) {

        } else {
            setListenerFilter(this);
        }
    }

    @Override
    public void filterResponse(String type, String searchTitle, int searchCityId, String searchCategory) {

    }
}

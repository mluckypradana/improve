package com.improve.app.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.improve.app.R;
import com.improve.app.activities.DetailConfirmationActivity;
import com.improve.app.activities.LoginActivity;
import com.improve.app.adapter.AdapterViewConfirmation;
import com.improve.app.model.ViewConfirmation;
import com.improve.app.service.response.ViewConfirmationResponse;
import com.improve.app.utils.Util;
import com.improve.app.widget.RotateLoading;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class ViewConfirmationFragment extends BaseFragment {

    RotateLoading rotateloading;
    TextView eventStatus;
//    SwipeRefreshLayout swipeContainer;
    ListView list;

    public ViewConfirmationFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    Context ctx;
    private AdapterViewConfirmation adapter;
    List<ViewConfirmation> listViewConfirm = new ArrayList<ViewConfirmation>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        ctx = getActivity();
        View v = inflater.inflate(R.layout.fragment_view_confirmation, container, false);
//        swipeContainer = (SwipeRefreshLayout) v.findViewById(R.id.swipeContainer);
        list = (ListView) v.findViewById(R.id.list);
        rotateloading = (RotateLoading) v.findViewById(R.id.rotateloading);
//        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                // Your code to refresh the list here.
//                // Make sure you call swipeContainer.setRefreshing(false)
//                // once the network request has completed successfully.
//
//                call_get_view_confirm();
//
//            }
//        });
//        // Configure the refreshing colors
//        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
//                android.R.color.holo_green_light,
//                android.R.color.holo_orange_light,
//                android.R.color.holo_red_light);

        adapter = new AdapterViewConfirmation(ctx, listViewConfirm, ViewConfirmationFragment.this);


        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(ctx, DetailConfirmationActivity.class);
                i.putExtra("id", listViewConfirm.get(position).getUser_id());
                i.putExtra("no_order", listViewConfirm.get(position).getNo_order());
                getActivity().startActivity(i);
            }
        });
        call_get_view_confirm();
        ButterKnife.bind(this, v);
        return v;
    }

    private void call_get_view_confirm() {
        if (Util.getUserId(getActivity()) == null || Util.getUserId(getActivity()).equals("") || getUserId() == 0) {
            Intent i = new Intent(getActivity(), LoginActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            getActivity().startActivity(i);
        } else {
            getViewConfirmation();
        }
    }

    private void showProgressDialog() {
        rotateloading.start();

        list.setVisibility(View.GONE);
        rotateloading.setVisibility(View.VISIBLE);

        /*mProgressDialog.setMessage("Please Wait...");
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();*/
    }

    private void dismissProgressDialog() {
        rotateloading.stop();

        list.setVisibility(View.VISIBLE);
        rotateloading.setVisibility(View.GONE);

        //mProgressDialog.dismiss();
    }

    private void getViewConfirmation() {
        //Show dialog
        showProgressDialog();
        Call<ViewConfirmationResponse> call;

        call = getService().get_view_confirmation(Util.getUserId(ctx));

        //Call API
        call.enqueue(new Callback<ViewConfirmationResponse>() {
            @Override
            public void onResponse(Response<ViewConfirmationResponse> response, Retrofit retrofit) {
                dismissProgressDialog();
                try {
                    ShowToast(response.body().getMessage());
                    listViewConfirm.clear();
                    listViewConfirm.addAll(response.body().getData());

                    adapter.updateReceiptsList(response.body().getData());
                    adapter.notifyDataSetChanged();
//                    swipeContainer.setRefreshing(false);
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
            }
        });

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}

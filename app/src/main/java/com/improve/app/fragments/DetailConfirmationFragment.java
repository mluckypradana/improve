package com.improve.app.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.improve.app.R;
import com.improve.app.dao.DetailSertifikasi;
import com.improve.app.service.response.ViewConfirmationResponse;
import com.improve.app.widget.RotateLoading;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by   Rian Erlangga Saputra
 * Email        rianerlangga03@gmail.com
 */
public class DetailConfirmationFragment extends BaseFragment {

    String id_confirm, no_order;


    List<DetailSertifikasi> detailSertifikasiList = new ArrayList<>();
    DetailSertifikasi detailSertifikasi;
    Context ctx;
    @Bind(R.id.rotateloading)
    RotateLoading rotateloading;
    @Bind(R.id.etMethod)
    EditText etMethod;
    @Bind(R.id.etAmount)
    EditText etAmount;
    @Bind(R.id.btnConfirm)
    Button btnConfirm;
    @Bind(R.id.rlMain)
    RelativeLayout rlMain;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View viewroot = inflater.inflate(R.layout.fragment_confirmation_payment, container, false);
        ctx = getActivity();
        ButterKnife.bind(this, viewroot);
        id_confirm = getActivity().getIntent().getStringExtra("id");
        no_order = getActivity().getIntent().getStringExtra("no_order");
        callPayment();


        return viewroot;
    }

    @OnClick(R.id.btnConfirm)
    void payment() {
        callPayment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void showProgressDialog() {
        rotateloading.start();

        rlMain.setVisibility(View.GONE);
        rotateloading.setVisibility(View.VISIBLE);

        /*mProgressDialog.setMessage("Please Wait...");
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();*/
    }

    private void dismissProgressDialog() {
        rotateloading.stop();

        rlMain.setVisibility(View.VISIBLE);
        rotateloading.setVisibility(View.GONE);

        //mProgressDialog.dismiss();
    }

    private void callPayment() {
        //Show dialog
        showProgressDialog();
        Call<ViewConfirmationResponse> call;

        call = getService().payment_confirmation(id_confirm, no_order, etMethod.getText().toString(), etAmount.getText().toString());

        //Call API
        call.enqueue(new Callback<ViewConfirmationResponse>() {
            @Override
            public void onResponse(Response<ViewConfirmationResponse> response, Retrofit retrofit) {
                dismissProgressDialog();
                try {
                    ShowToast(response.body().getMessage());

                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}

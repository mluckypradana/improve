package com.improve.app.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.improve.app.R;
import com.improve.app.activities.CartListActivity;
import com.improve.app.activities.HomeNewActivity;
import com.improve.app.activities.ProfileActivity;
import com.improve.app.activities.ViewConfirmationActivity;
import com.improve.app.utils.Cons;
import com.improve.app.widget.RotateLoading;


/**
 * Created by   yudi rahmat
 * Email        yudirahmat7@gmail.com
 */
public class ProfileMenuFragment extends BaseFragment {
    private SharedPreferences sharedPreference;
    private SharedPreferences.Editor editor;

    private RotateLoading mRotateLoading;
    private TextView mEventStatus;
    private ListView mListView;

    private Activity context;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        context     = activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View viewroot = inflater.inflate(R.layout.fragment_cartlist, container, false);
        return viewroot;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        sharedPreference	    = getActivity().getSharedPreferences(Cons.PRIVATE_PREF, Context.MODE_PRIVATE);
        editor				    = sharedPreference.edit();

        mListView               = (ListView) getView().findViewById(R.id.list);
        mEventStatus            = (TextView) getView().findViewById(R.id.event_status);
        mRotateLoading          = (RotateLoading) getView().findViewById(R.id.rotateloading);

        showListMenu();
    }

    private void showListMenu() {
        String[] menuArr = {"List Carts", "Konfirmasi Event", "Profil"};

        mEventStatus.setVisibility(View.GONE);
        mRotateLoading.setVisibility(View.GONE);
        mListView.setVisibility(View.VISIBLE);

        ArrayAdapter<String> mAdapter = new ArrayAdapter<String>(getActivity(), R.layout.item_list_listview, menuArr);
        mListView.setAdapter(mAdapter);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(position == 0) {
                    startNewActivity2(CartListActivity.class);
                } else if(position == 1) {
                    startNewActivity2(ViewConfirmationActivity.class);
                } else {
                    startNewActivity2(ProfileActivity.class);
                }
            }
        });
    }
}

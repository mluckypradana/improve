package com.improve.app.fragments;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.widget.Spinner;

import com.improve.app.activities.BaseActivity;
import com.improve.app.service.ApiService;
import com.improve.app.utils.Cons;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.ResponseBody;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okio.Buffer;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;


/**
 * Created by   yudi rahmat
 * Email        yudirahmat7@gmail.com
 */
public class BaseDialogFragment extends DialogFragment {
    private static Activity context;

    private ApiService service;
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        context = activity;
    }
    public ApiService getService() {
        return service;
    }
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(5, TimeUnit.MINUTES);
        client.setReadTimeout(5, TimeUnit.MINUTES);
        client.interceptors().add(new LoggingInterceptor());
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Cons.API_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        service = retrofit.create(ApiService.class);

        //Initialize SQLite database helper
//        helper = new DatabaseHelper(this);
    }

    public static class LoggingInterceptor implements Interceptor {
        @Override
        public Response intercept(Chain chain) throws IOException {
            Request request = chain.request();

            long t1 = System.nanoTime();
            String requestLog = String.format("Sending request %s on %s%n%s",
                    request.url(), chain.connection(), request.headers());
            //YLog.d(String.format("Sending request %s on %s%n%s",
            //        request.url(), chain.connection(), request.headers()));
            if (request.method().compareToIgnoreCase("post") == 0) {
                requestLog = "\n" + requestLog + "\n" + bodyToString(request);
            }
            Log.d("TAG", "request" + "\n" + requestLog);

            Response response = chain.proceed(request);
            long t2 = System.nanoTime();

            String responseLog = String.format("Received response for %s in %.1fms%n%s",
                    response.request().url(), (t2 - t1) / 1e6d, response.headers());

            String bodyString = response.body().string();

            Log.d("TAG", "response" + "\n" + responseLog + "\n" + bodyString);

            return response.newBuilder()
                    .body(ResponseBody.create(response.body().contentType(), bodyString))
                    .build();
            //return response;
        }
    }

    public static String bodyToString(final Request request) {
        try {
            final Request copy = request.newBuilder().build();
            final Buffer buffer = new Buffer();
            copy.body().writeTo(buffer);
            return buffer.readUtf8();
        } catch (final IOException e) {
            return "did not work";
        }
    }

    /*public SQLiteDatabase getDatabase() {
        return ((BaseActivity) getActivity()).getDatabase();
    }*/

    public void ShowToast(String text) {
        ((BaseActivity) getActivity()).ShowToast(text);
    }

    public void showDialogMessage(String title, String message) {
        ((BaseActivity) getActivity()).showDialogMessage(title, message);
    }

    public void startNewActivity(Class<?> cls) {
        ((BaseActivity) getActivity()).startNewActivity(cls);
    }

    public String getLatitude() {
        return ((BaseActivity) getActivity()).getLatitude();
    }

    public String getLongitude() {
        return ((BaseActivity) getActivity()).getLongitude();
    }

    public int getUserId() {
       return ((BaseActivity) getActivity()).getUserId();
    }

    public void getSharedPreferences() {
        ((BaseActivity) getActivity()).getSharedPreferences();
    }

    public void setSpinnerAdapter(Spinner spinners, String[] list) {
        ((BaseActivity) getActivity()).setSpinnerAdapter(spinners, list);
    }

    public String getImagePath(Uri imageUri) {
        return ((BaseActivity) getActivity()).getImagePath(imageUri);
    }

}

package com.improve.app.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.improve.app.R;
import com.improve.app.activities.LoginActivity;
import com.improve.app.dao.Comments;
import com.improve.app.dao.DetailPelatihan;
import com.improve.app.dao.TrainingFacilities;
import com.improve.app.service.response.CreateCartResponse;
import com.improve.app.service.response.DetailPelatihanResponse;
import com.improve.app.utils.Cons;
import com.improve.app.utils.Util;
import com.improve.app.widget.RotateLoading;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by   Rian Erlangga Saputra
 * Email        rianerlangga03@gmail.com
 */
public class DetailPelatihanFragment extends BaseFragment {

    RotateLoading rotateloading;
    @Bind(R.id.iv_logo_eo)
    ImageView ivLogoEo;
    @Bind(R.id.tv_penyelenggara)
    TextView tvPenyelenggara;
    @Bind(R.id.tv_penyelenggara_value)
    TextView tvPenyelenggaraValue;
    @Bind(R.id.tv_kategori)
    TextView tvKategori;
    @Bind(R.id.tv_kategori_value)
    TextView tvKategoriValue;
    @Bind(R.id.tv_event_title)
    TextView tvEventTitle;
    @Bind(R.id.iv_event_image)
    ImageView ivEventImage;
    @Bind(R.id.iv_event_image_shadow)
    ImageView ivEventImageShadow;
    @Bind(R.id.ly_event_image)
    RelativeLayout lyEventImage;
    @Bind(R.id.iv_event_ticket_capacity)
    ImageView ivEventTicketCapacity;
    @Bind(R.id.tv_event_ticket_capacity)
    TextView tvEventTicketCapacity;
    @Bind(R.id.iv_event_ticket_available)
    ImageView ivEventTicketAvailable;
    @Bind(R.id.tv_event_ticket_available)
    TextView tvEventTicketAvailable;
    @Bind(R.id.iv_event_ticket_registered)
    ImageView ivEventTicketRegistered;
    @Bind(R.id.tv_event_ticket_registered)
    TextView tvEventTicketRegistered;
    @Bind(R.id.ly_event_ticket)
    LinearLayout lyEventTicket;
    @Bind(R.id.iv_event_ticket_price)
    ImageView ivEventTicketPrice;
    @Bind(R.id.tv_event_ticket_price)
    TextView tvEventTicketPrice;
    @Bind(R.id.ly_event_ticket_price)
    RelativeLayout lyEventTicketPrice;
    @Bind(R.id.tv_event_address_label)
    TextView tvEventAddressLabel;
    @Bind(R.id.tv_event_address_value)
    TextView tvEventAddressValue;
    @Bind(R.id.iv_event_maps_location)
    ImageView ivEventMapsLocation;
    @Bind(R.id.tv_event_date_label)
    TextView tvEventDateLabel;
    @Bind(R.id.tv_event_date_value)
    TextView tvEventDateValue;
    @Bind(R.id.tv_event_facilities_label)
    TextView tvEventFacilitiesLabel;
    @Bind(R.id.tv_event_description_label)
    TextView tvEventDescriptionLabel;
    ScrollView svDetailSeminar;
    @Bind(R.id.llFacilities)
    LinearLayout llFacilities;
    @Bind(R.id.llComments)
    LinearLayout llComments;
    int id_sertifikasi;

    List<DetailPelatihan> detailPelatihanList = new ArrayList<>();
    DetailPelatihan detailPelatihan;
    Context ctx;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View viewroot = inflater.inflate(R.layout.fragment_detail_seminar, container, false);
        ctx = getActivity();

        setHasOptionsMenu(true);
        rotateloading = (RotateLoading) viewroot.findViewById(R.id.rotateloading);
        svDetailSeminar = (ScrollView) viewroot.findViewById(R.id.svDetailSeminar);
        id_sertifikasi = getActivity().getIntent().getIntExtra(Cons.PACKAGE_PREFIX + "pelatihanId", 1);
        get_detail_pelatihan();


        ButterKnife.bind(this, viewroot);
        return viewroot;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_daftar, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_daftar:
                call_create_cart();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void call_create_cart() {
        if (Util.getUserId(getActivity()) == null || Util.getUserId(getActivity()).equals("") || getUserId() == 0) {
            Intent i = new Intent(getActivity(), LoginActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            getActivity().startActivity(i);
        }else{
            create_cart();
        }
    }

    private void create_cart() {
        //Show dialog
        showProgressDialog();
        String user_id,item_count,type_program,program_id;
        user_id = Util.getUserId(getActivity());
        item_count = "1";
        type_program = Cons.PELATIHAN;
        program_id = id_sertifikasi+"";
        Call<CreateCartResponse> call;


        call = getService().create_cart(user_id,item_count,type_program,program_id);

        //Call API
        call.enqueue(new Callback<CreateCartResponse>() {
            @Override
            public void onResponse(Response<CreateCartResponse> response, Retrofit retrofit) {
                dismissProgressDialog();
                try {
                    ShowToast(response.body().getMessage());

                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
            }
        });

    }
    private void showProgressDialog() {
        rotateloading.start();

        svDetailSeminar.setVisibility(View.GONE);
        rotateloading.setVisibility(View.VISIBLE);

        /*mProgressDialog.setMessage("Please Wait...");
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();*/
    }

    private void dismissProgressDialog() {
        rotateloading.stop();

        svDetailSeminar.setVisibility(View.VISIBLE);
        rotateloading.setVisibility(View.GONE);

        //mProgressDialog.dismiss();
    }

    private void get_detail_pelatihan() {
        //Show dialog
        showProgressDialog();
        Call<DetailPelatihanResponse> call;

        call = getService().get_detail_pelatihan(id_sertifikasi + "");

        //Call API
        call.enqueue(new Callback<DetailPelatihanResponse>() {
            @Override
            public void onResponse(Response<DetailPelatihanResponse> response, Retrofit retrofit) {
                dismissProgressDialog();
                try {
                    ShowToast(response.body().getMessage());
                    detailPelatihanList.addAll(response.body().getData());

                    initDetailSeminar();
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
            }
        });

    }

    private void initDetailSeminar() {
        try {
            detailPelatihan = detailPelatihanList.get(0);
            Picasso.with(ctx)
                    .load(Cons.API_URL + detailPelatihan.getLogo())
                    .error(R.drawable.icon_eo_sample)
                    .into(ivLogoEo);
            tvPenyelenggaraValue.setText(detailPelatihan.getCreator());
            tvKategoriValue.setText(detailPelatihan.getCategory());
            tvEventTitle.setText(detailPelatihan.getTitle());
            try {

                Picasso.with(ctx)
                        .load(Cons.API_URL + detailPelatihan.getPoster_file_name())
                        .error(R.drawable.icon_eo_sample)
                        .into(ivEventImage);
            } catch (Exception e) {
                e.printStackTrace();
            }
            tvEventTicketCapacity.setText(getResources().getString(R.string.label_event_kapasitas) + detailPelatihan.getAvailable()
                    + getResources().getString(R.string.label_event_orang));
            tvEventTicketAvailable.setText(getResources().getString(R.string.label_event_tersedia) + detailPelatihan.getAvailable()
                    + getResources().getString(R.string.label_event_orang));
            if (detailPelatihan.getRegistered() == null) {

                tvEventTicketRegistered.setText(getResources().getString(R.string.label_event_peserta) + 0
                        + getResources().getString(R.string.label_event_orang));
            } else {

                tvEventTicketRegistered.setText(getResources().getString(R.string.label_event_peserta) + detailPelatihan.getRegistered()
                        + getResources().getString(R.string.label_event_orang));
            }

            tvEventTicketPrice.setText(detailPelatihan.getPrice());
            tvEventAddressValue.setText(android.text.Html.fromHtml(detailPelatihan.getAddress()));
            try {
                tvEventDateValue.setText(detailPelatihan.getTraining_schedules().get(0).getDate_start()
                        + getResources().getString(R.string.label_sampai)
                        + detailPelatihan.getTraining_schedules().get(0).getDate_end());
            } catch (Exception e) {
                e.printStackTrace();
            }


            for (final TrainingFacilities facility : detailPelatihan.getTraining_facilities()) {

                View v = getActivity().getLayoutInflater().inflate(R.layout.item_seminar_facilities, null);

                TextView tvFacility = (TextView) v.findViewById(R.id.tvItemFacilities);
                tvFacility.setText(facility.getDescription());
                llFacilities.addView(v);

            }
            tvEventDescriptionLabel.setText(android.text.Html.fromHtml(detailPelatihan.getDescription()));

            for (final Comments comment : detailPelatihan.getComments()) {

                View v = getActivity().getLayoutInflater().inflate(R.layout.item_comment, null);

                ImageView ivComment = (ImageView) v.findViewById(R.id.ivComment);
                TextView tvName = (TextView) v.findViewById(R.id.tvName);
                TextView tvDate = (TextView) v.findViewById(R.id.tvDate);
                TextView tvComment = (TextView) v.findViewById(R.id.tvComment);

                Picasso.with(ctx)
                        .load(Cons.API_URL + comment.getPhoto())
                        .error(R.drawable.icon_eo_sample)
                        .into(ivComment);
                tvName.setText(comment.getUsername());
                tvDate.setText(comment.getCreated_at());
                tvComment.setText(comment.getComment());
                llComments.addView(v);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}

package com.improve.app.adapter;

/**
 * Created by   yudi rahmat
 * Email        yudirahmat7@gmail.com
 */

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.improve.app.R;
import com.improve.app.fragments.CartListFragment;
import com.improve.app.model.CartModel;
import com.improve.app.model.CitiesModel;

import java.util.ArrayList;

public class CartAdapter extends BaseAdapter{
    private LayoutInflater mInflater;
    private ArrayList<CartModel> mList;
    private Context ctx;

    private CartListener listenerCart;

    public interface CartListener {
        void DeleteCartListener(int id, String programName, int position);
    }

    public CartAdapter(Context c, CartListFragment fragment) {
        mInflater   = LayoutInflater.from(c);
        ctx         = c;
        listenerCart = (CartListener) fragment;
    }

    public void setArray(ArrayList<CartModel> mList) {
        this.mList	= mList;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView	            =  mInflater.inflate(R.layout.item_list_cart, null);

            holder                  = new ViewHolder();

            holder.mItemTitle 		= (TextView) convertView.findViewById(R.id.item_cart_title);
            holder.mItemCategory    = (TextView) convertView.findViewById(R.id.item_cart_category);
            holder.mItemPrice 		= (TextView) convertView.findViewById(R.id.item_cart_price);
            holder.mItemCancel      = (ImageView) convertView.findViewById(R.id.ivCancel);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final CartModel data = mList.get(position);

        holder.mItemTitle.setText(data.title);
        holder.mItemCategory.setText(data.category);
        holder.mItemPrice.setText("Rp. " + setValue(String.valueOf(data.total), "0"));

        holder.mItemCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmDelete(ctx, data, position);
            }
        });

        return convertView;
    }

    private void confirmDelete(Context context, final CartModel data, final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setMessage("Cancel Untuk Event Ini ?")
                .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        listenerCart.DeleteCartListener(data.id, data.category, position);
                    }
                });

        builder.create().show();
    }

    private String setValue(String data, String label) {
        String value    = "";

        if(data.equals("") || data.equals("null"))
            value       = label;
        else
            value       = data;

        return value;
    }

    class ViewHolder{
        TextView mItemTitle;
        TextView mItemCategory;
        TextView mItemPrice;
        ImageView mItemCancel;
    }
}

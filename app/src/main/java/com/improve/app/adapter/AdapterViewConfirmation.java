package com.improve.app.adapter;


import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.improve.app.R;
import com.improve.app.fragments.ViewConfirmationFragment;
import com.improve.app.model.ViewConfirmation;

import java.util.ArrayList;
import java.util.List;


public class AdapterViewConfirmation extends BaseAdapter {

    Context ctx;
    LayoutInflater inflater;
    List<ViewConfirmation> listEkonomi = new ArrayList<ViewConfirmation>();

    ViewConfirmationFragment viewConfirmationFragment;

    private static class ViewHolder {
        public TextView tvNoOrder;
        public TextView tvPaymentState;
        public TextView tvHarga;
    }

    private List<ViewConfirmation> list = new ArrayList<ViewConfirmation>();


    public AdapterViewConfirmation(Context ctx, List<ViewConfirmation> posts, Fragment fragment) {
        this.ctx = ctx;
        this.inflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.list = posts;
//        this.aq = new AQuery(ctx);
        this.viewConfirmationFragment = (ViewConfirmationFragment) fragment;
    }

    public void updateReceiptsList(List<ViewConfirmation> newlist) {
        listEkonomi.clear();
        listEkonomi.addAll(newlist);
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressWarnings("deprecation")
    @Override
    public View getView(final int position, View v, ViewGroup parent) {

        final ViewHolder holder;

        if (v == null) {

            v = inflater.inflate(R.layout.item_list_view_confirmation, null);

        }

        if (v.getTag() == null) {
            holder = new ViewHolder();
            holder.tvHarga = (TextView) v.findViewById(R.id.tvHarga);
            holder.tvNoOrder = (TextView) v.findViewById(R.id.tvNoOrder);
            holder.tvPaymentState = (TextView) v.findViewById(R.id.tvPaymenState);
            v.setTag(holder);

        } else {
            holder = (ViewHolder) v.getTag();
        }
        final ViewConfirmation item = (ViewConfirmation) getItem(position);

        try {


            holder.tvHarga.setText(item.getTotal());
            holder.tvNoOrder.setText(item.getNo_order());
            holder.tvPaymentState.setText(item.getPayment_state());

        } catch (Exception e) {
            e.printStackTrace();
        }


        return v;
    }

}

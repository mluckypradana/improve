package com.improve.app.adapter;

/**
 * Created by   yudi rahmat
 */

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.swipe.adapters.BaseSwipeAdapter;
import com.improve.app.R;
import com.improve.app.fragments.CartListFragment;
import com.improve.app.model.CartModel;
import com.improve.app.utils.Util;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class CartSwipeAdapter extends BaseSwipeAdapter {
    //private LayoutInflater mInflater;
    private ArrayList<CartModel> mList;
    private Context ctx;

    private CartListener listenerCart;

    public interface CartListener {
        void DeleteCartListener(int id, String programName, int position);
    }

    public CartSwipeAdapter(CartListFragment fragment, ArrayList<CartModel> mList) {
        //mInflater   = LayoutInflater.from(c);
        ctx             = fragment.getActivity();
        listenerCart    = (CartListener) fragment;
        this.mList	    = mList;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    @Override
    public View generateView(int position, ViewGroup parent) {
        return LayoutInflater.from(ctx).inflate(R.layout.item_list_cart_swipe, null);
    }

    @Override
    public void fillValues(final int position, View convertView) {
        TextView mItemTitle 		= (TextView) convertView.findViewById(R.id.item_cart_title);
        TextView mItemCategory      = (TextView) convertView.findViewById(R.id.item_cart_category);
        TextView mItemPrice 		= (TextView) convertView.findViewById(R.id.item_cart_price);
        TextView mItemCancel2 		= (TextView) convertView.findViewById(R.id.tv_cancel_cart);
        ImageView mItemCancel       = (ImageView) convertView.findViewById(R.id.ivCancel);

        final CartModel data = mList.get(position);

        mItemTitle.setText(Util.capitalLetter(data.title.toLowerCase()));
        mItemCategory.setText(data.category + " - ");
        mItemPrice.setText("Rp. " + setValue(String.valueOf(data.total), "0"));

        mItemCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmDelete(ctx, data, position);
            }
        });

        mItemCancel2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmDelete(ctx, data, position);
            }
        });
    }

    private void confirmDelete(final Context context, final CartModel data, final int position) {
        /*AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setMessage("Cancel Untuk Event Ini ?")
                .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        listenerCart.DeleteCartListener(data.program_id, data.category, position);
                    }
                });

        builder.create().show();*/

        new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                .setTitleText(context.getResources().getString(R.string.label_dialog_title))
                .setContentText(Util.capitalLetter(data.title.toLowerCase()))
                .setCancelText(context.getResources().getString(R.string.label_dialog_cancel))
                .setConfirmText(context.getResources().getString(R.string.label_dialog_delete))
                .showCancelButton(true)
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        // reuse previous dialog instance, keep widget user state, reset them if you need
                        sDialog.setTitleText(context.getResources().getString(R.string.label_dialog_canceled))
                                .setContentText(context.getResources().getString(R.string.label_dialog_data_canceled))
                                .setConfirmText(context.getResources().getString(R.string.label_dialog_ok))
                                .showCancelButton(false)
                                .setCancelClickListener(null)
                                .setConfirmClickListener(null)
                                .changeAlertType(SweetAlertDialog.ERROR_TYPE);

                    }
                })
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        /*sDialog.setTitleText(context.getResources().getString(R.string.label_dialog_deleted))
                                .setContentText(context.getResources().getString(R.string.label_dialog_data_deleted))
                                .setConfirmText(context.getResources().getString(R.string.label_dialog_ok))
                                .showCancelButton(false)
                                .setCancelClickListener(null)
                                .setConfirmClickListener(null)
                                .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);*/

                        sDialog.dismiss();
                        listenerCart.DeleteCartListener(data.program_id, data.category, position);
                    }
                })
                .show();
    }

    private String setValue(String data, String label) {
        String value    = "";

        if(data.equals("") || data.equals("null"))
            value       = label;
        else
            value       = data;

        return value;
    }
}
package com.improve.app.adapter;

/**
 * Created by   yudi rahmat
 * Email        yudirahmat7@gmail.com
 * Company      TRUSTUDIO
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.improve.app.R;
import com.improve.app.model.EventPelatihan ;
import com.improve.app.utils.Cons;
import com.improve.app.utils.Util;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class PelatihanAdapter extends BaseAdapter{
    private LayoutInflater mInflater;
    private ArrayList<EventPelatihan > mList;
    private Context ctx;

    public PelatihanAdapter(Context c) {
        mInflater   = LayoutInflater.from(c);
        ctx         = c;
    }

    public void setArray(ArrayList<EventPelatihan > mList) {
        this.mList	= mList;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView	            =  mInflater.inflate(R.layout.item_list_event_pelatihan, null);

            holder                  = new ViewHolder();

            holder.mItemLogo            = (ImageView) convertView.findViewById(R.id.iv_logo_creator);
            holder.mItemTitle 		    = (TextView) convertView.findViewById(R.id.item_event_eo_name);
            holder.mItemDate 			= (TextView) convertView.findViewById(R.id.item_event_date);
            holder.mItemDesc 			= (TextView) convertView.findViewById(R.id.item_event_desc);
            holder.mItemAvailable 		= (TextView) convertView.findViewById(R.id.item_event_available);
            holder.mItemRegistered 		= (TextView) convertView.findViewById(R.id.item_event_registered);
            holder.mItemComments 		= (TextView) convertView.findViewById(R.id.item_event_comments);

            holder.mItemDate1           = (TextView) convertView.findViewById(R.id.tv_date_1);
            holder.mItemDate2           = (TextView) convertView.findViewById(R.id.tv_date_2);
            holder.mItemClass           = (TextView) convertView.findViewById(R.id.tv_class_event);

            holder.mItemLine            = (View) convertView.findViewById(R.id.line_top);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        EventPelatihan  data = mList.get(position);

        holder.mItemTitle.setText(data.title);
        holder.mItemDate.setText(data.city_name); //getDate(mList, position)
        holder.mItemDesc.setText(setValue(data.description, "Tidak Ada Deskripsi"));
        holder.mItemAvailable.setText(setValue(data.registered, "0") + "/" + setValue(data.available, "0"));
        holder.mItemRegistered.setText(setValue(data.registered, "0"));
        holder.mItemComments.setText(setValue(String.valueOf(data.comments), "0"));

        holder.mItemClass.setText(categoryProgram(data.category_program.toLowerCase()));

        try {
            int month   = convertDatetoArray(data.listSchedule.get(0).date_start.substring(0, 10))[1];

            holder.mItemDate1.setText(convertDatetoArray(data.listSchedule.get(0).date_start.substring(0, 10))[2] + "");
            holder.mItemDate2.setText(Util.months[month-1].substring(0, 3));
        } catch (Exception e) {
            e.printStackTrace();

            holder.mItemDate1.setText("");
            holder.mItemDate2.setText("");
        }

        try {
            Picasso.with(ctx)
                    .load(Cons.API_URL + data.logo)
                    .error(R.drawable.icon_load_image_failed)
                    .into(holder.mItemLogo);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(position == 0)
            holder.mItemLine.setVisibility(View.VISIBLE);
        else
            holder.mItemLine.setVisibility(View.GONE);

        return convertView;
    }

    private String categoryProgram(String category) {
        if(category.equals("profesional"))
            category    = "I";
        else if(category.equals("umum"))
            category    = "III";
        else
            category    ="II";

        return category;
    }

    private int[] convertDatetoArray(String time) {
        try {
            String split[]  = time.split("\\-");

            int year        = Integer.parseInt(split[0]);
            int month       = Integer.parseInt(split[1]);
            int day         = Integer.parseInt(split[2]);

            return new int[] {year, month, day};
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private String convertDate(String times) {
        String time = "";

        try {
            String split[]  = times.split("\\-");

            int year        = Integer.parseInt(split[0]);
            int month       = Integer.parseInt(split[1]);
            int day         = Integer.parseInt(split[2]);

            time            = day + " " + Util.months[month-1].substring(0, 3) + " " + year;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return time;
    }

    private String setValue(String data, String label) {
        String value    = "";

        if(data.equals("") || data.equals("null"))
            value       = label;
        else
            value       = data;

        return value;
    }

    private String getDate(ArrayList<EventPelatihan > mList, int position) {
        String date     = "";

        int length      = mList.get(position).listSchedule.size();

        if(mList.get(position).listSchedule != null && length > 0) {
            if(length > 1) {
                date    = convertDate(mList.get(position).listSchedule.get(0).date_start.substring(0, 10)) + " s/d " + convertDate(mList.get(position).listSchedule.get(length - 1).date_end.substring(0, 10));
            } else {
                date    = convertDate(mList.get(position).listSchedule.get(0).date_start.substring(0, 10));
            }
        }

        return date;
    }

    class ViewHolder{
        ImageView mItemLogo;
        TextView mItemTitle;
        TextView mItemDate;
        TextView mItemDesc;
        TextView mItemAvailable;
        TextView mItemRegistered;
        TextView mItemComments;
        View mItemLine;

        TextView mItemDate1;
        TextView mItemDate2;
        TextView mItemClass;
    }
}

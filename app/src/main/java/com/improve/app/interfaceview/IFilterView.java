package com.improve.app.interfaceview;

/**
 * Created by APCLAP002 on 5/14/2016.
 */
public interface IFilterView {
    void filterResponse(String type, String searchTitle, int searchCityId, String searchCategory);
}
